<?php

/* 
 * Manage Pages by Links
 * 
 */
add_action('admin_menu','register_pages_management');
function register_pages_management()
{
    add_menu_page('Theme Page Management',
                  'Theme Page Management',
                  'manage_options',
                  'dm-pages',
                  'dm_manage_settings');
    add_action('admin_init','dm_page_manage_settings');
}

function dm_page_manage_settings()
{
    register_setting('dm-pages-settings','dm-partners-page');
    register_setting('dm-pages-settings','dm-about-page');
    register_setting('dm-pages-settings','dm-tac-page');
    register_setting('dm-pages-settings','dm-pp-page');
    register_setting('dm-pages-settings','dm-coe-page');
}

function dm_manage_settings()
{
?>
    <div class="wrap1">
        <h2>Theme Page Management</h2>
        <form method="post" action="options.php">
        <?php wp_nonce_field('update-options'); ?>
        <table class="form-table">
            <tr valign="top">
            <th scope="row">Partners Page</th>
            <td><input type="text" 
                       name="dm-partners-page"
                       value="<?php print get_option('dm-partners-page') ?>"/></td>
            </tr>
            <tr valign="top">
            <th scope="row">About Page</th>
            <td><input type="text" 
                       name="dm-about-page"
                       value="<?php print get_option('dm-about-page') ?>"/></td>
            </tr>
            <tr valign="top">
            <th scope="row">Terms and Conditions</th>
            <td><input type="text" 
                       name="dm-tac-page"
                       value="<?php print get_option('dm-tac-page') ?>"/></td>
            </tr>
            <tr valign="top">
            <th scope="row">Privacy Policy</th>
            <td><input type="text" 
                       name="dm-pp-page"
                       value="<?php print get_option('dm-pp-page') ?>"/></td>
            </tr>
            <tr valign="top">
            <th scope="row">Code of Ethics</th>
            <td><input type="text" 
                       name="dm-coe-page"
                       value="<?php print get_option('dm-coe-page') ?>"/></td>
            </tr>
        </table>
        <input type="hidden" 
               name="action" 
               value="update" />
        <input type="hidden" 
               name="page_options" 
               value="dm-partners-page,dm-about-page,dm-tac-page,dm-pp-page,dm-coe-page" />
        <p class="submit">
            <input type="submit" 
                   class="button-primary" 
                   value="<?php _e('Save Changes') ?>" />
        </p>
        </form>
    </div>
<?php 
}

