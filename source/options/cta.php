<?php

/* 
 * Manage CTA by HTML Rendering
 * 
 */

add_action('admin_menu','register_cta_management_options');
function register_cta_management_options()
{
    add_menu_page('Call to Action Management',
                  'Call to Action Management', 
                  'manage_options',
                  'dm-cta-manage',
                  'cta_manage_settings');
    
    //call register settings function
	add_action( 'admin_init', 'cta_manage_option_settings' );
}

function cta_manage_option_settings()
{
    register_setting('cta-manage-settings','dm-cta-modal');
    register_setting('cta-manage-settings','dm-cta-video-single');
    register_setting('cta-manage-settings','dm-cta-home');
}

function cta_manage_settings()
{
?>
    <div class="wrap">
        <h2>Call to Action Management</h2>
        <form method="post" action="options.php">
        <?php wp_nonce_field('update-options'); ?>
        <table class="form-table">
            <tr valign="top">
            <th scope="row">Manage Call To Action Modal</th>
            <td><textarea rows="10" 
                          cols="100"
                          name="dm-cta-modal"><?php echo get_option('dm-cta-modal'); ?></textarea></td>
            </tr>
            <tr valign="top">
            <th scope="row">Manage Call To Action In Video Settings</th>
            <td><textarea rows="10" 
                          cols="100"
                          name="dm-cta-video-single"><?php echo get_option('dm-cta-video-single'); ?></textarea></td>
            </tr>
            <tr valign="top">
            <th scope="row">Manage Call To Action Below Headline in Home</th>
            <td><textarea rows="10" 
                          cols="100"
                          name="dm-cta-home"><?php echo get_option('dm-cta-home'); ?></textarea></td>
            </tr>
        </table>
        <input type="hidden" 
               name="action" 
               value="update" />
        <input type="hidden" 
               name="page_options" 
               value="dm-cta-modal,dm-cta-video-single,dm-cta-home" />
        <p class="submit">
            <input type="submit" 
                   class="button-primary" 
                   value="<?php _e('Save Changes') ?>" />
        </p>
        </form>
    </div>
<?php
}