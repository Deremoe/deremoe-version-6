<?php
/**
 * Add Trending Articles
 */
add_action('admin_menu','register_pages_trending');
function register_pages_trending()
{
    add_menu_page(
        'Trending',
        'Trending',
        'manage_options',
        'dm-trending',
        'dm_manage_trending_settings'
    );
    add_action('admin_init','dm_trending_settings');
}

function dm_trending_settings()
{
    register_setting('dm-trending-settings','dm-trending-events');
    register_setting('dm-trending-settings','dm-trending-topics');
}

function dm_manage_trending_settings()
{
?>
    <div class="warp1">
        <h2>Trending Management</h2>
        <form method="post" action="options.php">
        <?php wp_nonce_field('update-options'); ?>
            <table class="form-table">
                <tr valign="top">
                    <th scope="row">Trending Events</th>
                    <td><input type="text"
                               name="dm-trending-events"
                               value="<?php print get_option('dm-trending-events') ?>"/></td>
                </tr>
                <tr valign="top">
                    <th scope="row">Trending Topics</th>
                    <td><input type="text"
                               name="dm-trending-topics"
                               value="<?php print get_option('dm-trending-topics') ?>"/></td>
                </tr>
            </table>
            <input type="hidden"
                   name="action"
                   value="update" />
            <input type="hidden"
                   name="page_options"
                   value="dm-trending-events,dm-trending-topics" />
            <p class="submit">
                <input type="submit"
                       class="button-primary"
                       value="<?php _e('Save Changes') ?>" />
        </form>
    </div>
<?php
}