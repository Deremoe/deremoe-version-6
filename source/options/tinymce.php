<?php
/*
 * Put UiKit in the
 */
function add_uikit_to_editor()
{
    $font_url = str_replace( ',', '%2C', '//fonts.googleapis.com/css?family=Roboto+Slab:700,400|Roboto:400,900,900italic,300' );
    add_editor_style('//cdnjs.cloudflare.com/ajax/libs/uikit/2.21.0/css/uikit.min.css');
    add_editor_style($font_url);
}
add_action( 'admin_init', 'add_uikit_to_editor' );

/*
 * Setup the buttons
 */
function add_uikit_buttons()
{
    add_filter( "mce_external_plugins", "filter_uikit_add_buttons" );
    add_filter( 'mce_buttons', 'filter_uikit_register_buttons' );
}

function filter_uikit_add_buttons($plugin_array)
{
    $plugin_array['uikit'] = get_template_directory_uri() . '/assets/js/tinymce.uikit.js';
    return $plugin_array;
}

function filter_uikit_register_buttons($buttons)
{
    array_push($buttons,'read');
    return $buttons;
}
add_action('admin_init','add_uikit_buttons');