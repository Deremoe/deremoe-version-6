<?php
add_action('init','create_post_type_topic');
function create_post_type_topic()
{
	$args = array(
		'labels' => array(
			'name'			=> 'Topics',
			'singular_name'	=> 'Topic',
		),
		'public'		=> true,
		'description'	=> 'Post type for collating articles into one place.',
		'supports' => array(
			'title',
			'editor',
			'author',
			'thumbnail',
            'revisions'
		),
		'has_archive'	=> true,
		'rewrite'		=> array(
			'slug' => 'topic'
		)
	);
	register_post_type('Topic',$args);
}