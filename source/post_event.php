<?php
add_action('init','create_post_type_events');
function create_post_type_events()
{
	$args = array(
		'labels' => array(
			'name'			=> 'Events',
			'singular_name'	=> 'Event',
		),
		'public'		=> true,
		'description'	=> 'Post type for collating articles into one place.',
		'supports' => array(
			'title',
			'editor',
			'author',
			'thumbnail',
            'revisions'
		),
		'has_archive'	=> true,
		'rewrite'		=> array(
			'slug' => 'event'
		)
	);
	register_post_type('event',$args);
}