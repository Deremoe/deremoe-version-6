<?php
add_action('init','create_post_type_series_review');
function create_post_type_series_review()
{
	$args = array(
		'labels' => array(
			'name'			=> 'Series Reviews',
			'singular_name'	=> 'Series Review',
		),
		'public'		=> true,
		'description'	=> 'List of Series reviewed on the site.',
                'taxonomies' => array('category'),
		'supports' => array(
			'title',
			'editor',
			'author',
			'thumbnail',
            'comments',
            'revisions'
		),
		'rewrite'		=> array(
			'slug' => 'series-reviews'
		)
	);
	register_post_type('d5-series-review',$args);
}