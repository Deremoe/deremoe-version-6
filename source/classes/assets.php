<?php
//Shortcut to Assets Folders
class Assets 
{   
    public static function img($name)
    {
        return self::_tempUrl().'/assets/img/'.$name;
    }
    
    public static function js($name)
    {
        return self::_tempUrl().'/assets/js/'.$name;
    }
    
    public static function css($name)
    {
        return self::_tempUrl().'/assets/css/'.$name;
    }
    
    private static function _tempUrl()
    {
        return get_bloginfo('template_url');
    }
    
}