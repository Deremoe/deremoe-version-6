<?php

function call_report_loop()
{
    header('Content-type:application/json');
    $rsp = array();

    $post_id    = intval($_GET['post_id']);
    $review_id  = get_post_meta($post_id,'eventReviews',true);

    $args = array(
        'p'             => $review_id,
        'post_status'   => 'publish'
    );

    $rsp['show'] = false;
    if($review_id !== 'null'){
        $q = new WP_Query($args);
        while($q->have_posts()){
            $q->the_post();
            $loop_id = get_the_ID();

            $rsp['title']       = get_the_title();
            $rsp['permalink']   = get_the_permalink();
            $rsp['feature']     = get_that_image($loop_id,true);

            if(is_null($rsp['feature']) || $rsp['feature'] === false){
                $rsp['feature'] = 'http://placehold.it/350x150?text=No+Image+Found';
            }

            $rsp['authors']     = coauthors(null,null,null,null,false);
            $rsp['date']        = get_the_date();
            $rsp['show']        = true;
            break;
        }
    }

    print json_encode($rsp);
    die();
}
add_action('wp_ajax_nopriv_call_report_loop','call_report_loop');
add_action('wp_ajax_call_report_loop','call_report_loop');

function call_video_links()
{
    header('Content-type:application/json');
    $rsp = array();

    $post_id        = intval($_GET['post_id']);
    $video_posts    = get_post_meta($post_id,'eventVideoLinks',true);
    $video_ids      = array_map('intval',$video_posts);

    if(is_array($video_posts)){
    $args = array(
        'post_type'     => array('d5-videos'),
        'post__in'      => $video_ids,
        'post_status'   => 'publish'
    );

    $x = 0;
    $q = new WP_Query($args);
    while($q->have_posts()){
        $q->the_post();
        $loop_id = get_the_ID();

        $rsp[$x]['title']       = get_the_title();
        $rsp[$x]['permalink']   = get_the_permalink();

        $youtube_id = get_post_meta($loop_id,'youtube_video_id',true);
        $rsp[$x]['feature']     = "//img.youtube.com/vi/$youtube_id/hqdefault.jpg";
        $x++;
    };
}

    wp_reset_postdata();
    print json_encode($rsp);
    die();
}
add_action('wp_ajax_nopriv_call_video_links','call_video_links');
add_action('wp_ajax_call_video_links','call_video_links');

function call_related_articles()
{
    header('Content-type:application/json');
    $rsp = array();

    $post_id            = intval($_GET['post_id']);
    $relevant_posts    = get_post_meta($post_id,'eventRelevantArticles',true);
    if(is_array($relevant_posts)){
        $relevant_post_ids  = array_map('intval',$relevant_posts);

        $args = array(
            'post__in'      => $relevant_post_ids,
            'post_status'   => 'publish',
            'post_per_page' => 9,
            'paged'         => (isset($_GET['page']))?intval($_GET['page']):1
        );

        $q = new WP_Query($args);
        $x = 0;

        while($q->have_posts()){
            $q->the_post();
            $loop_id = get_the_ID();
            $cat     = end(get_the_category($loop_id));

            $rsp[$x]['title']       = get_the_title();
            $rsp[$x]['permalink']   = get_the_permalink();
            $rsp[$x]['feature']     = get_that_image($loop_id,true);

            if(is_null($rsp[$x]['feature']) || $rsp[$x]['feature'] === false){
                $rsp[$x]['feature'] = 'http://placehold.it/350x150?text=No+Image+Found';
            }

            $rsp[$x]['author']      = coauthors(null,null,null,null,false);
            $rsp[$x]['date']        = get_the_date();
            $rsp[$x]['category']    = $cat;

            $x++;
        }
    }

    wp_reset_postdata();
    print json_encode($rsp);
    die();
}
add_action('wp_ajax_nopriv_call_related_articles','call_related_articles');
add_action('wp_ajax_call_related_articles','call_related_articles');

function call_important_articles()
{
    header('Content-type:application/json');
    $rsp = array();

    $post_id            = intval($_GET['post_id']);
    $important_posts     = get_post_meta($post_id,'eventImporantArticles',true);
    if(is_array($important_posts)){
        $relevant_post_ids  = array_map('intval',$important_posts);

        $args = array(
            'post__in'      => $relevant_post_ids,
            'post_status'   => 'publish',
        );

        $q = new WP_Query($args);
        $x = 0;

        while($q->have_posts())
        {
            $q->the_post();
            $loop_id = get_the_ID();
            $cat     = end(get_the_category($loop_id));

            $rsp[$x]['title']       = get_the_title();
            $rsp[$x]['permalink']   = get_the_permalink();
            $rsp[$x]['feature']     = get_that_image($loop_id,true);

            if(is_null($rsp[$x]['feature']) || $rsp[$x]['feature'] === false){
                $rsp[$x]['feature'] = 'http://placehold.it/350x150?text=No+Image+Found';
            }

            $rsp[$x]['author']      = coauthors(null,null,null,null,false);
            $rsp[$x]['date']        = get_the_date();
            $rsp[$x]['category']    = $cat;

            $x++;
        }
    }

    wp_reset_postdata();
    print json_encode($rsp);
    die();
}
add_action('wp_ajax_nopriv_call_important_articles','call_important_articles');
add_action('wp_ajax_call_important_articles','call_important_articles');

function call_map_venue()
{
    header('Content-type:application/json');

    $post_id = intval($_GET['post_id']);
    $map     = get_post_meta($post_id,'eventMapVenue',true);

    $map['url_address'] = urlencode($map['address']);
    $map['latlong']     = (string)$map['lat'] .','.(string)$map['lng'];

    $map['permalink']= "https://maps.google.com/maps?q=loc:".$map['latlong'];
    $map['full_url'] = "https://maps.googleapis.com/maps/api/staticmap?center=". $map['url_address'] ."&zoom=16&size=720x360&maptype=roadmap
&markers=color:red%7Clabel:A%7C".$map['latlong'];

    print json_encode($map);
    die();
}
add_action('wp_ajax_nopriv_call_map_venue','call_map_venue');
add_action('wp_ajax_call_map_venue','call_map_venue');

function call_event_loop()
{
    header('Content-Type:application/json');

    $rsp = array();
    $arg = array(
        'post_type'     => array('event'),
        'post_per_page' => 10,
        'status'        => 'publish',
        'paged'         => $_GET['page']
    );

    $q = new WP_Query($arg);
    $x = 0;

    while($q->have_posts()){
        $q->the_post();
        $loop_id = get_the_ID();

        $rsp[$x]['title']       = get_the_title();
        $rsp[$x]['feature']     = get_that_image($loop_id);
        $rsp[$x]['permalink']   = get_the_permalink();
        $rsp[$x]['post_type']   = get_post_type();

        $post_ids   = array();
        $merge1 = get_post_meta($loop_id,'eventReviews',true);
        $merge1 = (is_array($merge1))?$merge1:array();

        $merge2 = get_post_meta($loop_id,'eventVideoLinks',true);
        $merge2 = (is_array($merge2))?$merge2:array();

        $merge3 = get_post_meta($loop_id,'eventRelevantArticles',true);
        $merge3 = (is_array($merge3))?$merge3:array();

        $merge4 = get_post_meta($loop_id,'eventImporantArticles',true);
        $merge4 = (is_array($merge4))?$merge4:array();

        $post_ids = array_merge($merge1,$merge2,$merge3,$merge4);
        $post_ids = array_map('intval',$post_ids);

        $rsp[$x]['topics']  = call_home_format_trending($post_ids);
        $x++;
    }

    print json_encode($rsp);
    die();
}
add_action('wp_ajax_nopriv_call_event_loop','call_event_loop');
add_action('wp_ajax_call_event_loop','call_event_loop');

function call_event_search()
{
    $search = esc_attr($_GET['query']);
    $rsp    = array();
    $arg    = array(
        's'             => $search,
        'post_type'     => array('event'),
        'post_status'   => 'publish',
        'post_per_page' => 10
    );

    $q = new WP_Query($arg);
    $x = 0;
    while($q->have_posts()){
        $q->the_post();

        $loop_id = get_the_ID();

        $rsp[$x]['title']       = get_the_title();
        $rsp[$x]['feature']     = get_that_image($loop_id);
        $rsp[$x]['permalink']   = get_the_permalink();
        $rsp[$x]['post_type']   = get_post_type();

        $post_ids   = array();
        $merge1 = get_post_meta($loop_id,'eventReviews',true);
        $merge1 = (is_array($merge1))?$merge1:array();

        $merge2 = get_post_meta($loop_id,'eventVideoLinks',true);
        $merge2 = (is_array($merge2))?$merge2:array();

        $merge3 = get_post_meta($loop_id,'eventRelevantArticles',true);
        $merge3 = (is_array($merge3))?$merge3:array();

        $merge4 = get_post_meta($loop_id,'eventImporantArticles',true);
        $merge4 = (is_array($merge4))?$merge4:array();

        $post_ids = array_merge($merge1,$merge2,$merge3,$merge4);
        $post_ids = array_map('intval',$post_ids);

        $rsp[$x]['topics']  = call_home_format_trending($post_ids);
        $x++;
    };

    print json_encode($rsp);
    die();
}
add_action('wp_ajax_nopriv_call_event_search','call_event_search');
add_action('wp_ajax_call_event_search','call_event_search');