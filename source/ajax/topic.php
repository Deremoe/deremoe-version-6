<?php
function call_topic_related_article()
{
    header('Content-type:application/json');
    $rsp = array(
        'post'  => array(),
        'video' => array()
    );

    $post_id    = intval($_GET['post_id']);
    $topic      = get_post_meta($post_id,'topicPost',true);

    if(is_array($topic)){
        $topic_ids  = array_map('intval',$topic);
        $args = array(
            'post_type'     => array('post'),
            'post__in'      => $topic_ids,
            'post_status'   => 'publish'
        );

        $x = 0;
        $q = new WP_Query($args);
        while($q->have_posts()){
            $q->the_post();
            $loop_id = get_the_ID();
            $cat     = end(get_the_category($loop_id,true));

            $rsp['post'][$x]['title']    = get_the_title();
            $rsp['post'][$x]['permalink']= get_the_permalink();
            $rsp['post'][$x]['feature']  = get_that_image($loop_id,true);

            if(is_null($rsp['post'][$x]['feature']) || $rsp['post'][$x]['feature'] === false){
                $rsp['post'][$x]['feature'] = 'http://placehold.it/350x150?text=No+Image+Found';
            };

            $rsp['post'][$x]['author']  = coauthors(null,null,null,null,false);
            $rsp['post'][$x]['date']    = get_the_date();
            $rsp['post'][$x]['category']= $cat;

            $x++;
        }
        wp_reset_postdata();
    }

    $videos     = get_post_meta($post_id,'topicVideo',true);
    if(is_array($videos)){
        $video_ids  = array_map('intval',$videos);
        $args = array(
            'post_type'     => array('d5-videos'),
            'post__in'      => $video_ids,
            'post_status'   => 'publish'
        );

        $x = 0;
        $q = new WP_Query($args);
        while($q->have_posts()){
            $q->the_post();
            $loop_id = get_the_ID();

            $rsp['video'][$x]['title']       = get_the_title();
            $rsp['video'][$x]['permalink']   = get_the_permalink();

            $youtube_id = get_post_meta($loop_id,'youtube_video_id',true);
            $rsp['video'][$x]['feature']     = "//img.youtube.com/vi/$youtube_id/hqdefault.jpg";
            $x++;
        };

        wp_reset_postdata();
    }

    print json_encode($rsp);
    die();
}
add_action('wp_ajax_nopriv_call_topic_related_article','call_topic_related_article');
add_action('wp_ajax_call_topic_related_article','call_topic_related_article');

function call_topic_loop()
{
    header('Content-Type:application/json');

    $rsp = array();
    $arg = array(
        'post_type'     => array('event'),
        'post_per_page' => 10,
        'status'        => 'publish',
        'paged'         => $_GET['page']
    );

    $q = new WP_Query($arg);
    $x = 0;

    while($q->have_posts()){
        $q->the_post();
        $loop_id = get_the_ID();

        $rsp[$x]['title']       = get_the_title();
        $rsp[$x]['feature']     = get_that_image($loop_id);
        $rsp[$x]['permalink']   = get_the_permalink();
        $rsp[$x]['post_type']   = get_post_type();

        $post_ids   = array();
        $merge1 = get_post_meta($loop_id,'eventReviews',true);
        $merge1 = (is_array($merge1))?$merge1:array();

        $merge2 = get_post_meta($loop_id,'eventVideoLinks',true);
        $merge2 = (is_array($merge2))?$merge2:array();

        $merge3 = get_post_meta($loop_id,'eventRelevantArticles',true);
        $merge3 = (is_array($merge3))?$merge3:array();

        $merge4 = get_post_meta($loop_id,'eventImporantArticles',true);
        $merge4 = (is_array($merge4))?$merge4:array();

        $post_ids = array_merge($merge1,$merge2,$merge3,$merge4);
        $post_ids = array_map('intval',$post_ids);

        $rsp[$x]['topics']  = call_home_format_trending($post_ids);
        $x++;
    }

    print json_encode($rsp);
    die();
}
add_action('wp_ajax_nopriv_call_topic_loop','call_topic_loop');
add_action('wp_ajax_call_topic_loop','call_topic_loop');

function call_topic_search()
{
    $search = esc_attr($_GET['query']);
    $rsp    = array();
    $arg    = array(
        's'             => $search,
        'post_type'     => array('topic'),
        'post_status'   => 'publish',
        'post_per_page' => 10
    );

    $q = new WP_Query($arg);
    $x = 0;
    while($q->have_posts()){
        $q->the_post();

        $loop_id = get_the_ID();

        $rsp[$x]['title']       = get_the_title();
        $rsp[$x]['feature']     = get_that_image($loop_id);
        $rsp[$x]['permalink']   = get_the_permalink();
        $rsp[$x]['post_type']   = get_post_type();

        $post_ids   = array();
        $merge1 = get_post_meta($loop_id,'topicPost',true);
        $merge1 = (is_array($merge1))?$merge1:array();

        $merge2 = get_post_meta($loop_id,'topicVideo',true);
        $merge2 = (is_array($merge2))?$merge2:array();

        $post_ids = array_merge($merge1,$merge2);
        $post_ids = array_map('intval',$post_ids);

        $rsp[$x]['topics']  = call_home_format_trending($post_ids);
        $x++;
    };

    print json_encode($rsp);
    die();
}
add_action('wp_ajax_nopriv_call_topic_search','call_topic_search');
add_action('wp_ajax_call_topic_search','call_topic_search');