<?php

/* 
 * General Function calls for the theme
 */

function call_category_link()
{
    $cat = get_category_by_slug($_GET['data']);
    
    $rsp = array();
    $rsp['result'] = get_category_link($cat->term_id);
    
    print json_encode($rsp);
    die();
}
add_action('wp_ajax_nopriv_call_category_link','call_category_link');
add_action('wp_ajax_call_category_link','call_category_link');

function call_author_list()
{
    $args = array(
        'exclude_admin' => false, 
        'show_fullname' => false,
        'hide_empty'    => true,
        'echo'          => false,
        'style'         => 'list',
        'include'       => array(17,1,4,13)
    ); 
    
    $list_authors = wp_list_authors($args);
    
    $rsp = array();
    $rsp['result'] = $list_authors;
    
    print json_encode($rsp);
    die();
}
add_action('wp_ajax_nopriv_call_author_list','call_author_list');
add_action('wp_ajax_call_author_list','call_author_list');

function call_search()
{
    $search = esc_attr($_GET['query']);
    
    $rsp = array();
    $x   = 0;
    $arg = array(
        's'             => $search,
        'post_status'   => 'publish',
        'post_per_page' => 10
    );
    
    $q   = new WP_Query($arg);
    while($q->have_posts()){
        $q->the_post();
        
        $rsp[$x]['title']       = get_the_title();
        $rsp[$x]['permalink']   = get_the_permalink();
        
        $x++;
    }
    
    print json_encode($rsp);
    die();
}
add_action('wp_ajax_nopriv_call_search','call_search');
add_action('wp_ajax_call_search','call_search');