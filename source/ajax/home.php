<?php

//Call Headline
function call_headline()
{
    header('Content-Type:application/json');
    $rsp = array();
    
    $arg = array(
        'post_type'     => array('post','d5-series-review'),
        'post_status'   => 'publish',
        'posts_per_page' => 1,
        'meta_key'      => 'push_to_headline',
        'meta_value'    => true,
    );
    $q = new WP_Query($arg);
    $x = 0;
    while($q->have_posts()){
        $q->the_post();
        $post_id = get_the_ID();
        $cat     = end(get_the_category($post_id));
        
        $rsp[$x]['title']       = get_the_title();
        $rsp[$x]['permalink']   = get_the_permalink();
        $rsp[$x]['feature']     = get_that_image($post_id);
        $rsp[$x]['author']      = coauthors(NULL,NULL,NULL,NULL,FALSE);
        $rsp[$x]['date']        = get_the_date();
        $rsp[$x]['category']    = $cat;
        $x++;
        break;
    }
    
    print json_encode($rsp);
    die();
}
add_action('wp_ajax_nopriv_call_headline','call_headline');
add_action('wp_ajax_call_headline','call_headline');

function call_reviews()
{
    header('Content-Type:application/json');
    $rsp = array();
    
    $arg = array(
        'post_type'     => array('d5-series-review'),
        'post_status'   => 'publish',
        'posts_per_page' => 2
    );
    $q = new WP_Query($arg);
    $x = 0;
    while($q->have_posts()){
        $q->the_post();
        $post_id = get_the_ID();
        
        $rsp[$x]['title']       = get_the_title();
        $rsp[$x]['permalink']   = get_the_permalink();
        $rsp[$x]['feature']     = get_that_image($post_id);
        $rsp[$x]['author']      = coauthors(NULL,NULL,NULL,NULL,FALSE);
        $rsp[$x]['date']        = get_the_date();
        $rsp[$x]['category']['cat_name'] = "Series Review";
        $x++;
    }
    
    print json_encode($rsp);
    die();
}
add_action('wp_ajax_nopriv_call_reviews','call_reviews');
add_action('wp_ajax_call_reviews','call_reviews');

function call_posts()
{
    header('Content-Type:application/json');
    $rsp = array();
    
    $arg = array(
        'post_type'      => array('d5-series-review','post'),
        'post_status'    => 'publish',
        'posts_per_page' => 9,
        'paged'          => (isset($_GET['data']))?intval($_GET['data']):1
    );
    $q = new WP_Query($arg);
    $x = 0;
    
    while($q->have_posts()){
        $q->the_post();
        $post_id = get_the_ID();
        $cat     = end(get_the_category($post_id));
        
        $rsp[$x]['title']       = get_the_title();
        $rsp[$x]['permalink']   = get_the_permalink();
        $rsp[$x]['feature']     = get_that_image($post_id,true);
        $rsp[$x]['author']      = coauthors(NULL,NULL,NULL,NULL,FALSE);
        $rsp[$x]['date']        = get_the_date();
        $rsp[$x]['category']    = $cat;
        $x++;
    }
    
    print json_encode($rsp);
    die();
}
add_action('wp_ajax_nopriv_call_posts','call_posts');
add_action('wp_ajax_call_posts','call_posts');

function call_video()
{
    header('Content-Type:application/json');
    $rsp = array();
    
    $arg = array(
        'post_type'      => array('d5-videos'),
        'post_status'    => 'publish',
        'posts_per_page' => 1,
    );
    $q = new WP_Query($arg);
    $x = 0;
    
    while($q->have_posts()){
        $q->the_post();
        
        $youtube_id     = get_post_meta(get_the_ID(),'youtube_video_id',true);
        $videoType      = get_post_meta(get_the_ID(),'d5VideoType',true);
        if(empty($videoType) || $videoType === 'youtube')
        {
            $rsp[$x]['url'] = "//www.youtube.com/embed/$youtube_id?autoplay=0&controls=0";
        } else {
            $rsp[$x]['url'] = "//www.facebook.com/video/embed?video_id=$youtube_id";
        }

        $x++;
    }
    
    print json_encode($rsp);
    die();
}
add_action('wp_ajax_nopriv_call_video','call_video');
add_action('wp_ajax_call_video','call_video');

function call_home_videos_links()
{
    header('Content-type:application/json');
    $rsp = array();
    $args = array(
        'post_type'     => array('d5-videos'),
        'post_status'   => 'publish',
        'posts_per_page' => 6
    );

    $x = 0;
    $q = new WP_Query($args);
    while($q->have_posts()){
        $q->the_post();
        $loop_id = get_the_ID();

        $rsp[$x]['title']       = get_the_title();
        $rsp[$x]['permalink']   = get_the_permalink();

        $youtube_id             = get_post_meta($loop_id,'youtube_video_id',true);
        $videoType              = get_post_meta($loop_id,'d5VideoType',true);
        if(empty($videoType) || $videoType == 'youtube' ){
            $rsp[$x]['feature']     = "//img.youtube.com/vi/$youtube_id/hqdefault.jpg";
        } else {
            $rsp[$x]['feature'] = get_that_image($loop_id,true);
        }

        $x++;
    };


    wp_reset_postdata();
    print json_encode($rsp);
    die();
}
add_action('wp_ajax_nopriv_call_home_videos_links','call_home_videos_links');
add_action('wp_ajax_call_home_videos_links','call_home_videos_links');

function call_home_trending()
{
    header('Content-type:application/json');
    $option_raw = get_option($_GET['option'],'dm-trending-events');
    $trending   = array_map('intval',explode(',',$option_raw));
    $args       = array(
        'post_type'     => array('event','Topic'),
        'post__in'      => $trending,
        'post_status'   => 'publish',
        'orderby'       => 'modifed',
        'order'         => 'DESC'
    );
    $q = new WP_Query($args);
    $x  = 0;
    $rsp= array();
    while($q->have_posts()){
        $q->the_post();
        $loop_id = get_the_ID();

        $rsp[$x]['title']       = get_the_title();
        $rsp[$x]['feature']     = get_that_image($loop_id);
        $rsp[$x]['permalink']   = get_the_permalink();
        $rsp[$x]['post_type']   = get_post_type();

        $post_ids   = array();
        if($rsp[$x]['post_type'] === 'event'){
            $merge1 = get_post_meta($loop_id,'eventReviews',true);
            $merge1 = (is_array($merge1))?$merge1:array();

            $merge2 = get_post_meta($loop_id,'eventVideoLinks',true);
            $merge2 = (is_array($merge2))?$merge2:array();

            $merge3 = get_post_meta($loop_id,'eventRelevantArticles',true);
            $merge3 = (is_array($merge3))?$merge3:array();

            $merge4 = get_post_meta($loop_id,'eventImporantArticles',true);
            $merge4 = (is_array($merge4))?$merge4:array();

            $post_ids = array_merge($merge1,$merge2,$merge3,$merge4);
            $post_ids = array_map('intval',$post_ids);
        } else {
            $merge1 = get_post_meta($loop_id,'topicPost',true);
            $merge1 = (is_array($merge1))?$merge1:array();

            $merge2 = get_post_meta($loop_id,'topicVideo',true);
            $merge2 = (is_array($merge2))?$merge2:array();

            $post_ids = array_merge($merge1,$merge2);
            $post_ids = array_map('intval',$post_ids);
        }
        $rsp[$x]['topics']  = call_home_format_trending($post_ids);
        $x++;
    }

    wp_reset_postdata();
    print json_encode($rsp);
    die();
}
add_action('wp_ajax_nopriv_call_home_trending','call_home_trending');
add_action('wp_ajax_call_home_trending','call_home_trending');

function call_home_format_trending($post_ids)
{
    $args = array(
        'post_type'     => array('post','d5-videos'),
        'post__in'      => $post_ids,
        'post_status'   => 'publish',
        'orderby'       => 'modified',
        'order'         => 'DESC',
        'posts_per_page' => '5'
    );
    $q  = new WP_Query($args);
    $x  = 0;
    $rsp= array();
    while($q->have_posts()){
        $q->the_post();
        $loop_id = get_the_ID();

        $rsp[$x]['permalink'] = get_the_permalink();
        $rsp[$x]['title']     = get_the_title();
        $rsp[$x]['post_type'] = get_post_type();
        if($rsp[$x]['post_type'] === 'd5-videos'){
            $rsp[$x]['title'] = '<span class="uk-badge uk-badge-danger">Video</span> '.$rsp[$x]['title'];
        }
        $x++;
    }
    return $rsp;
}