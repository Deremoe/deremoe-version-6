<?php

function call_video_loop()
{
    header('Content-Type:application/json');
    
    $rsp = array();
    $arg = array(
        'post_type'     => array('d5-videos'),
        'post_per_page' => 10,
        'status'        => 'publish',
        'paged'         => $_GET['page']
    );
    
    $q = new WP_Query($arg);
    $x = 0;
    
    while($q->have_posts()){
        $q->the_post();
        $post_id    = get_the_ID();
        $youtube_id = get_post_meta($post_id,'youtube_video_id',true);
        
        
        $rsp[$x]['title']       = get_the_title();
        $rsp[$x]['permalink']   = get_the_permalink();
        $videoType              = get_post_meta($post_id,'d5VideoType',true);
        if(empty($videoType)|| $videoType == 'youtube' ){
            $rsp[$x]['feature']     = "//img.youtube.com/vi/$youtube_id/hqdefault.jpg";
        } else {
            $rsp[$x]['feature'] = get_that_image($loop_id,true);
        }
        $rsp[$x]['date']        = get_the_date();
        $x++;
    }
    print json_encode($rsp);
    die();
}
add_action('wp_ajax_nopriv_call_video_loop','call_video_loop');
add_action('wp_ajax_call_video_loop','call_video_loop');

function call_more_article()
{
    header('Content-Type:application/json');
    $rsp = array();
    
    $arg = array(
        'post_type'      => array('d5-series-review','post'),
        'post_status'    => 'publish',
        'posts_per_page' => 5,
    );
    $q = new WP_Query($arg);
    $x = 0;
    
    while($q->have_posts()){
        $q->the_post();
        $post_id = get_the_ID();
        $cat     = end(get_the_category($post_id));
        
        $rsp[$x]['title']       = get_the_title();
        $rsp[$x]['permalink']   = get_the_permalink();
        $rsp[$x]['feature']     = get_that_image($post_id,true);
        $rsp[$x]['author']      = coauthors(NULL,NULL,NULL,NULL,FALSE);
        $rsp[$x]['date']        = get_the_date();
        $rsp[$x]['category']    = $cat;
        $rsp[$x]['page']        = $_GET;
        $x++;
    }
    
    print json_encode($rsp);
    die();
}
add_action('wp_ajax_nopriv_call_more_article','call_more_article');
add_action('wp_ajax_call_more_article','call_more_article');