<?php

function call_category_loop()
{
    header('Content-Type:application/json');

    $rsp = array();
    
    $arg = array(
        'category__in'  => array(strval($_GET['catId'])),
        'posts_per_page'=> 10,
        'post_status'   => 'publish',
        'paged'         => $_GET['page']
    );
    
    if(isset($_GET['isReview'])){
        $arg['post_type'] = array('d5-series-review','post');
    }
    
    $q = new WP_Query($arg);
    $x = 0;
    while($q->have_posts()){
        $q->the_post();
        $post_id = get_the_ID();
        
        $rsp[$x]['title']       = get_the_title();
        $rsp[$x]['permalink']   = get_the_permalink();
        $rsp[$x]['feature']     = get_that_image($post_id,true);
        $rsp[$x]['author']      = coauthors(NULL,NULL,NULL,NULL,FALSE);
        $rsp[$x]['date']        = get_the_date();
        $x++;
    }
    
    print json_encode($rsp);
    die();
}
add_action('wp_ajax_nopriv_call_category_loop','call_category_loop');
add_action('wp_ajax_call_category_loop','call_category_loop');
