<?php

function call_author_detail()
{
    $user_id = esc_attr($_GET['authorId']);
    $user    = get_user_by('id',strval($user_id));
    
    $rsp    = array();
    
    $rsp['author_name'] = $user->display_name;
    $rsp['user_email']  = $user->user_email;
    $rsp['description'] = get_the_author_meta('description',$user->ID);
    $rsp['twitter']     = get_the_author_meta('twitter',$user->ID);
    $rsp['website']     = get_the_author_meta('user_url',$user->ID);
    
    print json_encode($rsp);
    die();
}
add_action('wp_ajax_nopriv_call_author_detail','call_author_detail');
add_action('wp_ajax_call_author_detail','call_author_detail');

function call_author_posts()
{
    $user_id = esc_attr($_GET['authorId']);
    $page    = esc_attr($_GET['page']);
    
    $arg = array(
        'author'        => strval($user_id),
        'post_per_page' => 10,
        'post_status'   => 'publish',
        'paged'         => strval($page)
    );
    $q = new WP_Query($arg);
    $x = 0;
    while($q->have_posts()){
        $q->the_post();
        $post_id = get_the_ID();
        $cat     = end(get_the_category($post_id));
        
        $rsp[$x]['title']       = get_the_title();
        $rsp[$x]['permalink']   = get_the_permalink();
        $rsp[$x]['feature']     = get_that_image($post_id,true);
        $rsp[$x]['author']      = coauthors(NULL,NULL,NULL,NULL,FALSE);
        $rsp[$x]['date']        = get_the_date();
        $rsp[$x]['category']    = $cat;
        $x++;
    }

    print json_encode($rsp);
    die();
}
add_action('wp_ajax_nopriv_call_author_posts','call_author_posts');
add_action('wp_ajax_call_author_posts','call_author_posts');