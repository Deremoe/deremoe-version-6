<?php
add_action('init','create_post_type_videos');
function create_post_type_videos()
{
	$args = array(
		'labels' => array(
			'name'			=> 'Videos',
			'singular_name'	=> 'Video',
		),
		'public'		=> true,
		'description'	=> 'Post type specifically for writing with one video embeded',
		'supports' => array(
			'title',
			'editor',
			'author',
			'thumbnail',
            'comments',
            'revisions'
		),
		'has_archive'	=> true,
		'rewrite'		=> array(
			'slug' => 'videos'
		)
	);
	register_post_type('d5-videos',$args);
}