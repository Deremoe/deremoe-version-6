<?php
add_shortcode('flickr','short_flickr');
function short_flickr($atts)
{
    extract($atts);
    ob_start();
?>

<div style='position: relative;
            padding-bottom: 76%;
            height: 0;
            overflow: hidden;
            background: #2d2d2d;'>
    <iframe id='iframe'
            src='//flickrit.com/slideshowholder.php?height=75&size=big&setId=<?php print $id ?>&credit=1&trans=1&thumbnails=1&transition=1&layoutType=responsive&sort=0&count=<?php print $count ?>'
            scrolling='no'
            frameborder='0'
            style='width:100%;
                   height:100%;
                   position: absolute;
                   top:-20px;
                   left:0;'>
    </iframe>
</div>

<?php
}