<?php
/*
 * [badge class=""]content[/badge]
 */
function short_badge( $atts, $content = null )
{
    $a = shortcode_atts( array(
       'class' => '',
    ), $atts );
    return '<span class="uk-badge uk-badge-' . esc_attr($a['class']) . '">' . $content . '</span>';
};
add_shortcode( 'badge', 'short_badge' );

/*
 * [alert class=""]content[/alert]
 */
function short_alert($atts,$content = null)
{
    $args = array(
        'type' => ''
    );
    $param   = shortcode_atts($args,$atts);
    return '<alert type="'.$param['type'].'">'.$content.'</alert>';
}
add_shortcode( 'alert', 'short_alert' );