<?php 
    get_header();
    the_post();
    $post_id = get_the_ID();
?>
<link rel="stylesheet"
      id="dm6-videos-css"
      href="<?php print Assets::css('videos.css'); ?>"/>
<script src="<?php print Assets::js('vendor/share.js'); ?>"></script>
<main id="dm6-main-body"
      ng-controller="videosCtrl">
    <!-- Video Head -->
    <article id="dm6-video-container"
             class="uk-container 
                    uk-container-center
                    uk-margin-top">
    <header class="uk-margin-top">
        <h1 class="uk-text-header">
            <?php the_title(); ?>
        </h1>
        <section>
            <p>By <span class="uk-text-bold"><?php coauthors_links(); ?></span> on <?php the_date(); ?></p>
        </section>
    </header>
    <section id="dm6-video-body">
        <div class="videoWrapper">
            <?php $videoType = get_post_meta(get_the_ID(),'d5VideoType',true); ?>
            <?php if( empty($videoType) || $videoType == "youtube"): ?>
            <iframe width="1280"
                    height="720"
                    src="//www.youtube.com/embed/<?php print get_post_meta($post_id,'youtube_video_id',true) ?>?autoplay=0"
                    frameborder="0"
                    allowfullscreen>
            </iframe>
            <?php else: ?>
                <iframe src="//www.facebook.com/video/embed?video_id=<?php print get_post_meta($post_id,'youtube_video_id',true) ?>"
                        width="1280"
                        height="720"
                        frameborder="0"
                        allowfullscreen></iframe>
            <?php endif; ?>
        </div>
        <div id="dm6-video-excerpt" class="uk-margin-top">
            <?php the_content(); ?>
            <div class="uk-width-1-1 uk-clearfix">
                <div share-button 
                     class="share-button uk-float-right" ></div>
            </div>
        </div>
        <div id="dm6-video-footer">
            <!-- Video Loop -->
            <div class="uk-width-1-1" id="dm6-video-loop">
            <h2 class="uk-text-center">
                More Videos From Deremoe
            </h2>
            <hr/>
            <div class="uk-grid">
                <div class="uk-width-large-1-4
                            uk-width-medium-1-3
                            uk-width-small-1-1
                            uk-margin-bottom
                            video-loop"
                      ng-repeat="post in posts">
                    <a href="{{post.permalink}}">
                    <div style="background-image:url('{{post.feature}}')"
                         class="video-thumbnail">
                    <div class="content uk-vertical-align">
                        <div class="uk-vertical-align-middle uk-width-1-1">
                        <p class="uk-text-center uk-margin-remove">
                            <i class="uk-icon uk-icon-play-circle"></i>
                        </p>
                        </div>
                    </div>
                    <div class="video-title uk-width-9-10">
                        <p clamp-this-text line="2" ng-bind-html="post.title">
                        </p>
                    </div>
                    </div>
                    </a>
                </div>
                
                <div class="uk-width-large-1-4
                            uk-width-medium-1-3
                            uk-width-small-1-1
                            uk-margin-bottom
                            video-loop
                            video-loop-ad">
                    <a href="<?php print get_post_type_archive_link('d5-videos'); ?>">
                    <div class="video-thumbnail">
                    <div class="content uk-vertical-align">
                        <div class="uk-vertical-align-middle uk-width-1-1">
                        <p class="uk-text-center uk-margin-remove">
                            <i class="uk-icon uk-icon-play-circle"></i>
                        </p>
                        </div>
                    </div>
                    </div>
                    </a>
                </div>
                
            </div>
            </div>
            
            <!-- Call to Action -->
            <div class="uk-width-1-1 uk-margin-top uk-grid"
                 id="dm6-cta">
                <div class="uk-width-large-2-3
                            uk-width-medium-1-1 uk-margin-bottom">
                <?php print get_template_part('template/part','cta'); ?>
                </div>
                <div class="uk-width-large-1-3
                            uk-width-medium-1-1">
                    <?php print get_option('dm-cta-video-single') ?>
                </div>
            </div>
            <hr/>
            
            <!-- Comments and Article Loop -->
            <div class="uk-width-1-1 uk-margin-top uk-grid"
                 id="dm6-comment-body">
                <div class="uk-width-medium-4-6
                            uk-width-small-1-1">
                <?php comments_template(); ?>
                </div>
                <div class="uk-width-medium-2-6
                            uk-width-small-1-1">
                    <aside class="uk-panel uk-panel-box"
                           id="dm6-more-articles">
                    <h3 class="uk-panel-title 
                               uk-text-center 
                               uk-text-bold">
                    More Articles
                    </h3>
                    <ul class="uk-list">

                    <li class="dm6-articles uk-margin-bottom" 
                        ng-repeat="article in moreArticles">
                    <a href="{{article.permalink}}">
                    <div class="uk-width-1-1">
                    <div class="image"
                         style="background-image:url('{{article.feature}}')">
                        <div class="image-content uk-vertical-align">
                            <div class="uk-vertical-align-bottom">
                                <h3 ng-bind-html="article.title"></h3>
                            </div>
                        </div>
                    </div>
                    </div>
                    </a>
                    </li>
                    
                    </ul>
                    </aside>
                </div>
            </div>
        </div>
    </section>
    </article>
</main>

<?php get_footer(); ?>