<?php 
    get_header(); 
    the_post();
    $post_id = get_the_ID();
?>
<link rel="stylesheet"
      id="dm6-reviews-css"
      href="<?php print Assets::css('review.css'); ?>"/>
<script src="<?php print Assets::js('vendor/share.js'); ?>"></script>
<main id="dm6-main-body"
      ng-controller="reviewCtrl">
    <!-- Review Head -->
    <section id="dm6-review-cover" 
             class="uk-width-1-1"
             style="background-image:url('<?php print get_that_image($post_id); ?>')">
    <!-- Black Container -->
    <div class="uk-width-width-1-1 
                uk-vertical-align" 
         id="content">
        <div class="uk-width-1-1 
                    uk-vertical-align-middle
                    uk-text-center"
             id="title">
            <h1 class="uk-heading-large">
                <?php the_title() ?>
            </h1>
        </div>
    </div>
    
    <!-- Authors -->
    <div class="uk-width-1-4"
         id="authors">
        <ul>
        <?php foreach(coAuthorsCustom() as $author): ?>
            
            <li class="uk-margin-bottom 
                       uk-visible-large 
                       dm6-author-desktop 
                       uk-float-right">
                <a href="<?php print $author['author_url']; ?>"
                       class="uk-link-muted"
                       itemprop="author"
                       title="<?php print $author['nice_name']; ?>">
                <?php print get_avatar($author['author_id'],'50') ?>
                <span>
                    <?php print $author['nice_name']; ?>
                </span>
                </a>
            </li>
            
            <li class="uk-margin-bottom 
                       uk-hidden-large 
                       dm6-author-mobile 
                       uk-float-right">
                <a href="<?php print $author['author_url']; ?>"
                   class="uk-link-muted"
                   itemprop="author"
                   title="<?php print $author['nice_name']; ?>">
                <?php print get_avatar($author['author_id'],'50') ?>
               </a>
            </li>
        <?php endforeach;?>
        </ul>
    </div>
    
    <!-- Ratings -->
    <div class="uk-width-1-4"
         id="ratings"
         title="5/5">
    <ul class="uk-float-right">
    <?php foreach(reviewRating($post_id) as $rating): ?>
        <li>
            <i class="uk-icon <?php print $rating; ?>"></i>
        </li>
    <?php endforeach;?>
    </ul>
    </div>
    
    </section>
    
    <!-- Review Body -->
    <section class="uk-width-medium-6-10
                    uk-width-small-1-1
                    uk-container-center
                    uk-margin-top"
             id="dm6-review-body">
        <?php the_content(); ?>
    <div class="uk-clearfix">
        <div class="uk-grid">
            <div class="uk-width-medium-1-2 dm6-authors">
                <ul class="uk-list">
                    <?php foreach(coAuthorsCustom() as $author): ?>
                        <li class="uk-margin-bottom uk-width-1-1">
                            <?php print get_avatar($author['author_id'],'25') ?>
                            <span class="uk-text-normal">
                                <a href="<?php print $author['author_url']; ?>"
                                   class="uk-link-muted"
								   itemprop="author">
                                    <?php print $author['nice_name']; ?>
                                </a>
                                <?php if(!empty($author['twitter'])): ?>
                                |
                                <a href="http://twitter.com/<?php print $author['twitter']; ?>">
                                <i class="uk-icon uk-icon-twitter"></i>
                                @<?php print $author['twitter']; ?>
                                </a>
                                <?php endif; ?>
                                <?php if(!empty($author['email'])): ?>
                                    |
                                    <a href="mailto:<?php print $author['email']; ?>">
                                    <i class="uk-icon uk-icon-envelope"></i>
                                    Mail
                                    </a>
                                <?php endif; ?>
                            </span>
                        </li>
                    <?php endforeach;?>
                </ul>
            </div>
            <div class="uk-width-medium-1-2">
                <div class="share-button uk-float-right" 
                     share-button></div>
            </div>
        </div>
    </div>
    <hr/>
    <?php print get_template_part('template/part','cta'); ?>
    <hr/>
    <?php comments_template(); ?>
    </section>
</main>
<?php get_footer(); ?>