<?php get_header(); ?>
<link rel="stylesheet" 
      id="dm6-home-css" 
      href="<?php print Assets::css('home.css'); ?>"/>
<main id="dm6-main-body" 
      class="uk-margin-top"
      ng-controller="homeCtrl"
      ng-init="loadAjaxFunc()">
    
    <!-- Call to Action Modal With Cookie expire of 1 hour --> 
    <section id="dm6-cta-modal" class="uk-width-1-1" ng-show="ctaModal">
    <div id="cta-modal-container" 
         class="uk-width-1-1 uk-container-center">
        <div class="uk-container-center" id="cta-modal">
        <div class="uk-panel uk-panel-box">
            <div class="uk-width-1-1">
                <a href="#" 
                   class="uk-float-right uk-modal-close uk-close"
                   ng-click="ctaModalClose()">
                </a>
            </div>          
            <!-- Images and HTML here -->
            <?php print get_option('dm-cta-modal'); ?>
        </div>
        </div>
    </div>
        
    </section>
    <div class="uk-container uk-container-center">
    <div class="uk-grid" id="dm6-article-container">
    
    <!-- Header Image -->
    <section class="uk-width-1-1"
             id="dm6-headline-article">
	
        <a href="{{headline.permalink}}">
        <div class="header-image 
                    uk-width-1-1 
                    mobile-square-thumbnail 
                    header-image"
             style="background-image: url('{{headline.image}}')">
            <div class="article-container uk-vertical-align">
                <div class="dm6-category 
                            uk-text-bold 
                            uk-text-small">
                    {{headline.category}}
                </div>
                <div class="uk-vertical-align-bottom dm6-title2">
                <h2 class="uk-h1" ng-bind-html="headline.title">
                </h2>
                <p class="uk-text-small uk-margin-top-remove">
                    by {{headline.author}} &bull; {{headline.date}}
                </p>
                </div>
            </div>
        </div>
        </a>
    </section>
    
    <!-- Call to Action -->
    <div class="uk-width-1-1
                uk-margin-top
                uk-margin-bottom">
    <?php print get_option('dm-cta-home'); ?>
    <!-- Ad Start -->
    <div class="uk-margin-top uk-margin-bottom">
        <?php get_template_part('template/ad','flat'); ?>
    </div>
    </div>

    <!-- Trending Events -->
    <div class="uk-width-1-1
                uk-margin-bottom"
         id="dm-trending-events"
         ng-show="events.length > 0">
        <div class="uk-panel
                    uk-panel-box">
            <h1>
                <i class="uk-icon uk-icon-ticket"></i>
                Event News
            </h1>
            <div class="uk-grid"
                 data-uk-grid-match>
            <item-trending
                item="post"
                ng-repeat="post in events"></item-trending>

            <item-placeholder
                type="events"
                ng-show="events.length <= 2"></item-placeholder>
            <item-placeholder
                ng-show="events.length <= 1"
                type="events"></item-placeholder>
            </div>
        </div>
    </div>

    <div class="uk-width-1-1
                uk-margin-bottom"
         id="dm-trending-topics"
         ng-show="topics.length > 0">
        <div class="uk-panel
                    uk-panel-box">
            <h1>
                <i class="uk-icon uk-icon-bell"></i>
                Trending Topic
            </h1>
            <div class="uk-grid">
                <item-trending
                    item="post"
                    ng-repeat="post in topics"></item-trending>

                <item-placeholder
                    type="topics"
                    ng-show="events.length <= 2"></item-placeholder>
                <item-placeholder
                    ng-show="events.length <= 1"
                    type="topics"></item-placeholder>
            </div>
        </div>
    </div>

    <!-- Reviews -->
    <item-large item="review" ng-repeat="review in reviews"></item-large>
    
    <!-- Video -->
    <section class="uk-width-1-1 uk-margin-top">
    <div class="uk-grid" 
         data-uk-grid-match="{target:'.videoPanel'}"
         infinite-scroll="loadMorePost()"
         infinite-scroll-disabled='disableScroll'>

        <div class="uk-width-large-2-3
                    uk-width-medium-1-1
                    uk-margin-bottom">
            <div class="videoWrapper videoPanel">
            <iframe width="560" 
                    height="315" 
                    ng-src="{{yturl}}" 
                    frameborder="0" 
                    allowfullscreen></iframe>
            </div>
        </div>

        <div class="uk-width-large-1-3
                    uk-width-medium-1-1
                    uk-margin-bottom
                    uk-margin-top">
            <div class="videoPanel">
            <?php //get_template_part('template/ad','custom'); ?>
                <div class="uk-width-1-1">
                    <a href="http://tag77.com/tagvert/details/55c02114161f468314033714" target="_blank"
                       style="display:block;">
                        <img src="//deremoe.com/wp-content/uploads/2015/08/320px-x-320px.jpg"
                             class="uk-width-1-1"/>
                    </a>
                </div>
            </div>
        </div>

        <!-- Loop 6 videos -->
        <item-video item="video"
                    ng-repeat="video in videos"></item-video>

        <div class="uk-width-1-1 uk-text-center uk-margin-bottom">
            <a href="<?php print get_post_type_archive_link('d5-videos'); ?>"
               class="uk-button uk-button-large"
               style="background-color:#DF2826;color:#fff;"
               target="_blank">
                <i class="uk-icon uk-icon-play"></i>
                More Videos
            </a>
        </div>
        
        <!-- Loop 11 -->
        <item-basic item="post"
                    ng-repeat="post in posts"></item-basic>
    </div>
    </div>
    </section>
    </div>
        
    <div class="uk-width-1-1 uk-text-center uk-margin-top"
         ng-show="disableScroll == true">
        <i class="uk-icon uk-icon-spin uk-icon-spinner uk-icon-large"></i>
    </div>
</main>
<?php get_footer(); ?>