<?php get_header(); ?>
<link rel="stylesheet"
      id="dm6-archive-events-css"
      href="<?php print Assets::css('archive-events.css'); ?>"/>
<main id="dm6-archive-events"
      class="uk-margin-top"
      ng-controller="archiveEventsCtrl">
<div class="uk-width-1-1"
     id="dm6-event-advisory">
    <alert type="uk-alert-warning">
        We are currently on the process of migrating our posts for this category.
        If you are looking for our Event Reports archive, click
        <strong><a href="" cat-link slug="events">here</a></strong>.
    </alert>
</div>
<div class="uk-width-1-1"
     id="dm6-event-controls">
   <div class="uk-width-large-6-10
               uk-width-medium-8-10
               uk-width-small-1-1
               uk-container-center
               uk-text-center">
        <h1>Events Coverage</h1>
        <p>Covering events is among Deremoe's beats, and here is where you can find stories
            arranged for each event.</p>
        <p class="uk-margin-top">
            <input type="text"
                   placeholder="Type your query and press Enter"
                   ng-model="searchEventQuery"
                   ng-keydown="searchEvent($event)"/></p>
   </div>
   <div class="uk-width-1-1
               uk-text-center"
        ng-show="displayLoadingIcon">
       <i class="uk-icon
                 uk-icon-spin
                 uk-icon-refresh
                 uk-icon-large"></i>
   </div>
   <!-- Search Result -->
   <div class="uk-width-1-1"
        ng-show="displaySearchResult">
       <div class="uk-width-medium-8-10
                    uk-width-small-1-1
                    uk-container-center">
           <p class="uk-text-center">
               <button class="uk-button uk-button-danger"
                       ng-click="closeSearch()">
                   <i class="uk-icon uk-icon-close"></i>
                   Close Search
               </button>
           </p>
           <div class="uk-grid">
            <item-trending item="event"
                           ng-repeat="event in searchResults"></item-trending>
           </div>
       </div>
   </div>

   <!-- Item Result -->
   <div class="uk-width-1-1
               uk-margin-top"
        infinite-scroll="eventMoreLoop()"
        infinite-scroll-disable="displayLoadingIcon"
        ng-show="displayEventItems">
        <div class="uk-width-medium-8-10
                    uk-width-small-1-1
                    uk-container-center">
        <div class="uk-grid">
            <item-trending item="event"
                           ng-repeat="event in events"></item-trending>
        </div>
        </div>

   </div>

   <div class="uk-width-1-1
               uk-text-center"
        ng-show="displayLoadingIcon">
       <i class="uk-icon
                 uk-icon-spin
                 uk-icon-refresh
                 uk-icon-large"></i>
   </div>
</div>
</main>
<?php get_footer();?>