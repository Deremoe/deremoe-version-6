<?php get_header(); ?>
<?php
	the_post();
?>
<link rel="stylesheet"
      id="dm6-single-css"
      href="<?php print Assets::css('single.css'); ?>"/>
<main id="dm6-main-body"
      ng-controller="singleCtrl">
<div class="uk-width-large-6-10 
            uk-width-medium-1-1 
            uk-container-center">
    <header class="uk-margin-top" 
            id="dm6-single-header">
        <h1 class="uk-text-center">
            <?php the_title(); ?>
        </h1>
    </header>
    <div id="dm6-single-body" 
         class="uk-margin-top uk-text-break">
        <?php the_content(); ?>
    </div>
	<hr />
	<?php print get_template_part('template/part','cta'); ?>
</div>
</main>

<?php get_footer(); ?>