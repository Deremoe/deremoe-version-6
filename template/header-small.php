<header id="dm6-main-header-mobile"
        class="uk-hidden-large
               dm6-main-header
               uk-width-1-1"
        data-uk-sticky>
    <nav class="uk-navbar">
        <ul class="uk-navbar-nav">
            <li><a href="#dm-header-mobile"
                   data-uk-offcanvas><i class="uk-icon uk-icon-bars"></i></a></li>
            <li><a href="#searchModal"
                   class="uk-float-right"
                   data-uk-tooltip="{pos:'left'}"
                   title="Search The Site"
                   data-uk-modal>
                    <i class="uk-icon uk-icon-search"></i>
                </a>
            </li>
        </ul>
        <div class="uk-navbar-flip">
            <div class="uk-navbar-brand">
                <a href="<?php bloginfo('url'); ?>">
                    <img src="<?php print Assets::img('logo-title.svg'); ?>"/>
                </a>
            </div>
        </div>
    </nav>
</header>