<link rel="stylesheet" href="<?php print Assets::css('landing-topic.css'); ?>"/>
<!-- Cover -->
<div class="uk-width-1-1"
     id="dm6-cover-section">
    <div id="dm6-cover-info"
         class="uk-flex-center
                uk-flex-middle
                uk-flex
                uk-grid">
        <div class="uk-width-large-7-10
                    uk-width-medium-8-10
                    uk-width-small-9-10
                    uk-flex">
            <div class="uk-width-1-1">
                <h1><span><?php the_title(); ?></span></h1>
                <p><span><?php print get_post_meta(get_the_ID(),'topicExcerpt',true); ?></span></p>
            </div>
        </div>
    </div>
    <div class="filter"></div>
    <div id="dm6-landing-image"
         style="background-image:url('<?php print get_that_image(get_the_ID()); ?>')"></div>
</div>
<!-- Videos -->
<div class="uk-width-1-1">
    <div class="uk-width-large-7-10
                uk-width-medium-8-10
                uk-width-small-9-10
                uk-container-center
                uk-margin-top">
        <div class="uk-grid">
            <item-video item="video" ng-repeat="video in posts.video"></item-video>
        </div>
    </div>
</div>
<!-- Articles -->
<div class="uk-width-1-1">
    <div class="uk-width-large-7-10
                uk-width-medium-8-10
                uk-width-small-9-10
                uk-container-center
                uk-margin-top"
         id="dm-related-article">
        <div class="uk-grid">
            <item-basic item="post" ng-repeat="post in posts.post"></item-basic>
        </div>
    </div>
</div>