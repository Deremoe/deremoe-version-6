<div class="uk-visible-large">
    <header id="dm6-main-header"
            class="uk-width-1-1
                   dm6-main-header"
            data-uk-sticky>
        <nav class="uk-container
                uk-container-center">
            <div class="brand">
                <a href="<?php bloginfo('url'); ?>">
                    <img src="<?php print Assets::img('logo-title.svg'); ?>"/>
                </a>
            </div>
            <div class="menus"
                 id="dm6-main-menu">
                <ul class="uk-subnav
                           uk-subnav-line
                           uk-margin-remove">
                    <li data-uk-dropdown="{mode:'click'}">
                        <a href=""
                           cat-link slug="features">Features <i class="uk-icon uk-icon-caret-down"></i></a>
                        <div class="uk-dropdown uk-dropdown-small">
                            <ul class="uk-nav uk-nav-dropdown">
                                <li><a href="" cat-link slug="episode-blogging">Anime Blogging</a></li>
                                <li><a href="" cat-link slug="analysis">Analysis</a></li>
                                <li><a href="" cat-link slug="opinions">Opinions</a></li>
                                <li><a href="" cat-link slug="manga-pitch">Manga Pitch</a></li>
                            </ul>
                        </div>
                    </li>
                    <li><a href="<?php print get_post_type_archive_link('d5-videos'); ?>">Videos</a></li>
                    <li><a href="" cat-link slug="reviews">Reviews</a></li>
                    <li data-uk-dropdown="{mode:'click'}">
                        <a href="">Writers <i class="uk-icon uk-icon-caret-down"></i></a>
                        <div class="uk-dropdown uk-dropdown-small">
                            <ul class="uk-nav uk-nav-dropdown" ng-bind-html="authorList">
                            </ul>
                        </div>
                    </li>
                    <li><a href="<?php print get_post_type_archive_link('event'); ?>">Events</a></li>
                    <li><a href="<?php print get_post_type_archive_link('topic'); ?>">Topics</a></li>
                    <li data-uk-dropdown="{mode:'click'}">
                        <a href="">
                            <i class="uk-icon uk-icon-caret-down"></i>
                        </a>
                        <div class="uk-dropdown uk-dropdown-small">
                            <ul class="uk-nav uk-nav-dropdown">
                                <li><a href="" cat-link slug="podcast">Podcast</a></li>
                                <li><a href="<?php print get_option('dm-partners-page'); ?>"
                                       target="_new">Partners</a>
                                </li>
                                <li class="uk-nav-divider"></li>
                                <li class="uk-nav-header">Social</li>
                                <li>
                                    <a href="http://www.facebook.com/Deremoe" target="_blank">
                                        <i class="uk-icon uk-icon-facebook"></i>
                                        Facebook
                                    </a>
                                </li>
                                <li>
                                    <a href="http://twitter.com/Deremoe" target="_blank">
                                        <i class="uk-icon uk-icon-twitter"></i>
                                        Twitter
                                    </a>
                                </li>
                                <li>
                                    <a href="http://www.youtube.com/Deremoe" target="_blank">
                                        <i class="uk-icon uk-icon-youtube-play"></i>
                                        YouTube
                                    </a>
                                </li>
                                <li>
                                    <a href="http://plus.google.com/+Deremoe" target="_blank">
                                        <i class="uk-icon uk-icon-google-plus"></i>
                                        Google+
                                    </a>
                                </li>
                                <li>
                                    <a href="http://pinterest.com/Deremoe" target="_blank">
                                        <i class="uk-icon uk-icon-pinterest"></i>
                                        Pinterest
                                    </a>
                                </li>
                                <li>
                                    <a href="http://radar.deremoe.com" target="_blank">
                                        <i class="uk-icon uk-icon-tumblr"></i>
                                        Tumblr
                                    </a>
                                </li>
                                <li>
                                    <a href="https://www.flickr.com/photos/deremoe" target="_blank">
                                        <i class="uk-icon uk-icon-flickr"></i>
                                        Flickr
                                    </a>
                                </li>
                                <li>
                                    <a href="http://instagram.com/deremoe" target="_blank">
                                        <i class="uk-icon uk-icon-instagram"></i>
                                        Instagram
                                    </a>
                                </li>
                                <li>
                                    <a href="http://medium.com/featured-on-deremoe" target="_blank">
                                        <i class="uk-icon uk-icon-medium-logo"></i>
                                        Medium
                                    </a>
                                </li>
                                <li class="uk-nav-divider"></li>
                                <li><a href="<?php print get_option('dm-pp-page'); ?>">
                                        Privacy Policy</a>
                                </li>
                                <li><a href="<?php print get_option('dm-coe-page'); ?>">
                                        Ethics Statement
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </li>
                </ul>
            </div>

            <div class="search">
                <a href="#searchModal"
                   class="uk-float-right"
                   data-uk-tooltip="{pos:'left'}"
                   title="Search The Site"
                   data-uk-modal>
                    <i class="uk-icon uk-icon-search"></i>
                </a>
            </div>
        </nav>
    </header>
</div>