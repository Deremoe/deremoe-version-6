<div id="dm-header-mobile"
     class="uk-offcanvas">
    <div class="uk-offcanvas-bar">
        <ul class="uk-nav uk-nav-offcanvas uk-nav-parent-icon"
            data-uk-nav>
            <li class="uk-nav-header">
                Main Menu
            </li>
            <li class="uk-parent">
                <a href="#">Features</a>
                <ul class="uk-nav-sub">
                    <li><a href="" cat-link slug="episode-blogging">Anime Blogging</a></li>
                    <li><a href="" cat-link slug="analysis">Analysis</a></li>
                    <li><a href="" cat-link slug="opinions">Opinions</a></li>
                    <li><a href="" cat-link slug="manga-pitch">Manga Pitch</a></li>
                </ul>
            </li>
            <li><a href="<?php print get_post_type_archive_link('d5-videos'); ?>">Videos</a></li>
            <li><a href="<?php print get_post_type_archive_link('event'); ?>">Event News</a></li>
            <li><a href="<?php print get_post_type_archive_link('topic'); ?>">Topics</a></li>
            <li><a href="" cat-link slug="reviews">Reviews</a></li>
            <li class="uk-parent">
                <a href="#">Writers</a>
                <ul class="uk-nav-sub"
                    ng-bind-html="authorList">
                </ul>
            </li>
            <li><a href="" cat-link slug="events">Event Reports</a></li>
            <li><a href="" cat-link slug="podcast">Podcast</a></li>
            <li><a href="<?php print get_option('dm-partners-page'); ?>"
                   target="_new">
                    Partners
                </a>
            </li>
            <li><a href="<?php print get_option('dm-pp-page'); ?>"
                   target="_new">
                    Privacy Statement
                </a>
            </li>
            <li><a href="<?php print get_option('dm-coe-page'); ?>"
                   target="_new">
                    Ethics
                </a>
            </li>
            <li class="uk-nav-header">
                Social
            </li>
            <li>
                <a href="http://www.facebook.com/Deremoe" target="_blank">
                    <i class="uk-icon uk-icon-facebook"></i>
                    Facebook
                </a>
            </li>
            <li>
                <a href="http://twitter.com/Deremoe" target="_blank">
                    <i class="uk-icon uk-icon-twitter"></i>
                    Twitter
                </a>
            </li>
            <li>
                <a href="http://www.youtube.com/Deremoe" target="_blank">
                    <i class="uk-icon uk-icon-youtube-play"></i>
                    YouTube
                </a>
            </li>
            <li>
                <a href="http://plus.google.com/+Deremoe" target="_blank">
                    <i class="uk-icon uk-icon-google-plus"></i>
                    Google+
                </a>
            </li>
            <li>
                <a href="http://pinterest.com/Deremoe" target="_blank">
                    <i class="uk-icon uk-icon-pinterest"></i>
                    Pinterest
                </a>
            </li>
            <li>
                <a href="http://radar.deremoe.com" target="_blank">
                    <i class="uk-icon uk-icon-tumblr"></i>
                    Tumblr
                </a>
            </li>
            <li>
                <a href="https://www.flickr.com/photos/deremoe" target="_blank">
                    <i class="uk-icon uk-icon-flickr"></i>
                    Flickr
                </a>
            </li>
            <li>
                <a href="http://instagram.com/deremoe" target="_blank">
                    <i class="uk-icon uk-icon-instagram"></i>
                    Instagram
                </a>
            </li>
            <li>
                <a href="http://medium.com/featured-on-deremoe" target="_blank">
                    <i class="uk-icon uk-icon-medium-logo"></i>
                    Medium
                </a>
            </li>
        </ul>
    </div>
</div>