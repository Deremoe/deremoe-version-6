<aside class="uk-grid dm6-cta">
	<div class="uk-width-large-1-2 uk-margin-bottom">
		<strong class="uk-h3">Join the Deremoe Community</strong>
		<p class="uk-margin-remove">Follow us on our social platforms for more content. Watch our videos, answer our questions and polls, and know the latest updates from us.</p>
	</div>
	<div class="uk-width-large-1-2">
		<div class="uk-grid">
			<div class="uk-width-1-2">
				<div class="uk-width-1-1">
					<a href="https://twitter.com/Deremoe" class="twitter-follow-button" data-show-count="false" data-dnt="true">Follow @Deremoe</a>
					<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
				</div>
				<div class="uk-width-1-1">
					<div id="fb-root"></div>
					<script>(function(d, s, id) {
					  var js, fjs = d.getElementsByTagName(s)[0];
					  if (d.getElementById(id)) return;
					  js = d.createElement(s); js.id = id;
					  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.4&appId=198528793520783";
					  fjs.parentNode.insertBefore(js, fjs);
					}(document, 'script', 'facebook-jssdk'));</script>
					<div class="fb-like" data-href="https://facebook.com/deremoe" data-layout="button_count" data-action="like" data-show-faces="false" data-share="false"></div>
				</div>
				<div class="uk-width-1-1">
					<a data-pin-do="buttonFollow" href="http://www.pinterest.com/Deremoe/">Follow Deremoe</a>
					<!-- Please call pinit.js only once per page -->
					<script type="text/javascript" async defer src="//assets.pinterest.com/js/pinit.js"></script>
				</div>
				<div class="uk-width-1-1">
					<iframe class="btn" border="0" scrolling="no" allowtransparency="true" src="http://platform.tumblr.com/v1/follow_button.html?button_type=1&tumblelog=deremoe&color_scheme=dark" frameborder="0" height="25" width="144"></iframe>
				</div>				
			</div>
			<div class="uk-width-1-2">
				<div class="uk-width-1-1">
					<script src="https://apis.google.com/js/platform.js"></script>
					<div class="g-ytsubscribe" data-channel="Deremoe" data-layout="default" data-theme="dark" data-count="default"></div>
				</div>
				<div class="uk-width-1-1">
					<!-- Place this tag in your head or just before your close body tag. -->
					<script src="https://apis.google.com/js/platform.js" async defer></script>

					<!-- Place this tag where you want the widget to render. -->
					<div class="g-follow" data-annotation="bubble" data-height="20" data-href="https://plus.google.com/107398008535995533732" data-rel="publisher"></div>		
				</div>
				<div class="uk-width-1-1">
					<a href='http://cloud.feedly.com/#subscription%2Ffeed%2Fhttp%3A%2F%2Fderemoe.com%2Ffeed'  target='blank'><img id='feedlyFollow' src='http://s3.feedly.com/img/follows/feedly-follow-rectangle-flat-small_2x.png' alt='follow us in feedly' width='66' height='20'></a>
				</div>
				<div class="uk-width-1-1">
					<style>.ig-b- { display: inline-block; }
					.ig-b- img { visibility: hidden; }
					.ig-b-:hover { background-position: 0 -60px; } .ig-b-:active { background-position: 0 -120px; }
					.ig-b-v-24 { width: 137px; height: 24px; background: url(//badges.instagram.com/static/images/ig-badge-view-sprite-24.png) no-repeat 0 0; }
					@media only screen and (-webkit-min-device-pixel-ratio: 2), only screen and (min--moz-device-pixel-ratio: 2), only screen and (-o-min-device-pixel-ratio: 2 / 1), only screen and (min-device-pixel-ratio: 2), only screen and (min-resolution: 192dpi), only screen and (min-resolution: 2dppx) {
					.ig-b-v-24 { background-image: url(//badges.instagram.com/static/images/ig-badge-view-sprite-24@2x.png); background-size: 160px 178px; } }</style>
					<a href="http://instagram.com/deremoe?ref=badge" class="ig-b- ig-b-v-24"><img src="//badges.instagram.com/static/images/ig-badge-view-24.png" alt="Instagram" /></a>
				</div>				
			</div>
			
		</div>
	</div>
  </aside>