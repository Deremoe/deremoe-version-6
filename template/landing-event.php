<link rel="stylesheet" href="<?php print Assets::css('landing-event.css'); ?>"/>
<!-- Cover -->
<div class="uk-width-1-1"
     id="dm6-cover-section">
    <div id="dm6-cover-info"
         class="uk-flex-center
            uk-flex-middle
            uk-flex
            uk-grid">
        <div class="uk-width-large-7-10
                uk-width-medium-8-10
                uk-width-small-9-10
                uk-flex
                uk-grid"
             data-uk-grid-match>
            <div class="uk-width-medium-1-3
                        uk-width-small-1-1
                        uk-text-center"
                 id="poster">
                <a href="<?php print getPosterImage(get_the_ID()); ?>"
                   rel="lightbox[9999]"
                   target="_blank">
                    <img src="<?php print getPosterImage(get_the_ID()); ?>"/>
                </a>
            </div>
            <div class="uk-width-medium-2-3
                        uk-width-small-1-1"
                 id="info">
                <h1><span class="black">
                <?php the_title(); ?>
                    </span></h1>
                <h2 class="uk-margin-remove">
                    <span class="red">
                        #<?php print get_post_meta(get_the_ID(),'eventHashtag',true) ?>
                    </span>
                </h2>
                <div class="uk-margin-large-top uk-grid uk-hidden-small"
                     id="star-article"
                     ng-show="posts.main_report.show">
                    <div class="uk-width-medium-1-2 important">
                        <a style="background-image: url('{{posts.main_report.feature}}')"
                           ng-href="{{posts.main_report.permalink}}"
                           target="_blank">
                        <div class="uk-flex">
                            <div class="uk-margin-bottom">
                                <h3>{{posts.main_report.title}}</h3>
                            </div>
                        </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="filter"></div>
    <div id="dm6-landing-image"
         style="background-image:url('<?php print get_that_image(get_the_ID()) ?>')"></div>
</div>

<!-- Trailer -->
<div class="uk-width-1-1">
    <div class="uk-width-large-7-10
                uk-width-medium-8-10
                uk-width-small-9-10
                uk-container-center
                uk-margin-top
                padding"
        id="dm-trailer-info">
        <div class="uk-grid uk-flex uk-flex-middle"
             data-uk-grid-match>
            <div class="uk-width-medium-2-3
                        uk-width-small-1-1
                        uk-margin-bottom
                        dm-trailer">
                <?php $eventTrailer = get_post_meta(get_the_ID(),'eventVideoTrailer',true); ?>
                <?php if(!empty($eventTrailer)): ?>
                <div class="videoWrapper videoPanel">
                    <iframe width="560"
                            height="560"
                            src="//www.youtube.com/embed/<?php print get_post_meta(get_the_ID(),'eventVideoTrailer',true) ?>"
                            frameborder="0"
                            allowfullscreen></iframe>
                </div>
                <?php else: ?>
                <a ng-href="{{posts.map.permalink}}"
                   target="_blank">
                    <img ng-src="{{posts.map.full_url}}"/>
                </a>
                <?php endif; ?>
            </div>
            <div class="uk-width-medium-1-3
                        uk-width-small-1-1
                        dm-info">
                <div class="uk-block">
                    <div class="uk-container">
                        <dl class="uk-description-list-line">
                            <dt>
                                <i class="uk-icon uk-icon-calendar"></i>
                                Date
                            </dt>
                            <dd><?php print get_post_meta(get_the_ID(),'eventDate',true) ?></dd>
                            <dt>
                                <i class="uk-icon uk-icon-map-marker"></i>
                                Venue
                            </dt>
                            <dd><?php print get_post_meta(get_the_ID(),'eventVenue',true) ?></dd>
                            <dt>
                                <i class="uk-icon uk-icon-users"></i>
                                Organization
                            </dt>
                            <dd><?php print get_post_meta(get_the_ID(),'eventOrganization',true) ?></dd>
                        </dl>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Improtant Posts -->
<div class="uk-width-1-1">
    <div class="uk-width-large-7-10
                uk-width-medium-8-10
                uk-width-small-9-10
                uk-container-center
                uk-margin-top
                padding"
         id="dm-important-loop">
    <div class="uk-grid">
        <item-large item="post"
                    ng-repeat="post in posts.important_posts"></item-large>
    </div>
    </div>
</div>

<!-- Videos -->
<div class="uk-width-1-1">
    <div class="uk-width-large-7-10
                uk-width-medium-8-10
                uk-width-small-9-10
                uk-container-center
                uk-margin-top
                padding"
        id="dm-video-loop">
        <h2>Videos</h2>
        <div class="uk-grid uk-flex uk-flex-middle">
            <item-video item="video"
                        ng-repeat="video in posts.video_links"></item-video>
        </div>
    </div>
</div>

<!-- Related Articles -->
<div class="uk-width-1-1">
    <div class="uk-width-large-7-10
                uk-width-medium-8-10
                uk-width-small-9-10
                uk-container-center
                uk-margin-top
                padding"
         id="dm-related-article">
        <h2>Related Articles</h2>
        <div class="uk-grid"
             infinite-scroll="loadMorePost()"
             infinite-scroll-disabled="disableScroll">
            <item-basic item="post" ng-repeat="post in posts.related_posts"></item-basic>
            <div class="uk-width-1-1 uk-text-center uk-margin-top"
                 ng-show="disableScroll">
                <i class="uk-icon uk-icon-spin uk-icon-spinner uk-icon-large"></i>
            </div>
        </div>
    </div>
</div>