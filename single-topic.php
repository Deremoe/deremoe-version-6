<?php
    get_header();
    the_post();
?>
<div ng-controller="landingTopicCtrl"
     ng-init="setPostId(<?php print get_the_ID(); ?>)">
    <?php get_template_part('template/landing','topic'); ?>
</div>
<?php
    get_footer();
?>