<?php get_header(); ?>
<link rel="stylesheet" 
      id="dm6-home-css" 
      href="<?php print Assets::css('home.css'); ?>">
<main id="dm6-main-body" 
      class=""
      ng-controller="homeCtrl">
    <div class="uk-container 
         uk-container-center">
    <div class="uk-text-center">
        <h1>Uh-oh, we have a 4-oh-4.</h1>
        <p>The page you are looking for might have been eaten by parasites (or titans), got sucked into a different dimension, or (as some say) cannot be found - chances are, <strong>it can't be found</strong>, and we're sorry for that.</p>
        <p>You might want to check out the latest posts that we have instead.</p>
    </div>
    <div class="uk-grid" 
         id="dm6-article-container"
         infinite-scroll="loadMorePost()"
         infinite-scroll-disabled='disableScroll'>
        <!-- Loop 11 -->
        <section class="uk-width-large-1-3
                        uk-width-medium-1-2 
                        uk-width-small-1-1
                        uk-margin-top
                        dm6-loop-normal"
                 ng-repeat="post in posts">
        <a href="{{post.permalink}}">
            <div class="header-image"
                     style="background-image: url('{{post.feature}}')">
                <div class="article-container uk-vertical-align">
                    <div class="dm6-category 
                            uk-text-bold 
                            uk-text-small">
                    {{post.category.category_nicename}}
                    </div>
                    <div class="uk-vertical-align-bottom dm6-title2">
                        <h2 class="uk-margin-remove" ng-bind-html="post.title">
                        </h2>
                    <p class="uk-text-small">
                       by {{post.author}} &bull;- {{post.date}}
                    </p>
                    </div>
                </div>
            </div>
        </a>
        </section>
    
    <div class="uk-width-1-1 uk-text-center uk-margin-top" ng-show="disableScroll == true">
        <i class="uk-icon uk-icon-spin uk-icon-spinner uk-icon-large"></i>
    </div>
    
    </div>
    </div>
</main>
<?php get_footer(); ?>