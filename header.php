<!DOCTYPE html>
<html <?php language_attributes(); ?> ng-app="dm6">
<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" 
          content="width=device-width, 
                   height=device-width, 
                   initial-scale=1.0, 
                   maximum-scale=1.0, 
                   user-scalable=no">
    
    <!-- FAVICON -->
    <link href="/favicon.ico" 
		  rel="shortcut icon" />	

    <!-- For iPad with high-resolution Retina display running iOS ≥ 7: -->
    <link rel="apple-touch-icon-precomposed" 
          sizes="152x152" 
          href="/apple-icon-152x152.png">

    <!-- For iPad with high-resolution Retina display running iOS ≤ 6: -->
    <link rel="apple-touch-icon-precomposed" 
          sizes="144x144" 
          href="/apple-icon-144x144.png">

    <!-- For iPhone with high-resolution Retina display running iOS ≥ 7: -->
    <link rel="apple-touch-icon-precomposed" 
          sizes="120x120" 
          href="/apple-icon-120x120.png">

    <!-- For iPhone with high-resolution Retina display running iOS ≤ 6: -->
    <link rel="apple-touch-icon-precomposed" 
          sizes="114x114" 
          href="/apple-icon-114x114.png">

    <!-- For first- and second-generation iPad: -->
    <link rel="apple-touch-icon-precomposed" 
          sizes="72x72" 
          href="/apple-icon-72x72.png">

    <!-- For non-Retina iPhone, iPod Touch, and Android 2.1+ devices: -->
    <link rel="apple-touch-icon-precomposed" 
          href="/apple-icon.png">
	<link rel="manifest" href="/manifest.json"/>
	
	<!-- For Windows 8 devices: -->
	<meta name="msapplication-TileColor" content="#ab1f2b"/>
	<meta name="msapplication-TileImage" content="/ms-icon-310x310.png"/>
	<meta name="theme-color" content="#ffffff"/>
    
    <title><?php wp_title('&bull;',true,'right'); ?></title>
    <?php wp_head(); ?>
    <script>
        var ajaxurl         = "<?php print admin_url('admin-ajax.php'); ?>";
        var directiveUrl    = "<?php print bloginfo('template_url').'/template/directives/'?>";
    </script>
</head>
<body class="uk-flex">
<!-- Header Item -->
<div ng-controller="headerCtrl"
     class="uk-width-1-1"
     id="dm-header-container">

    <!-- Header Desktop -->
    <?php get_template_part('template/header','large'); ?>

    <!-- Header Mobile -->
    <?php get_template_part('template/header','small'); ?>

    <!-- Mobile Header Menu -->
    <?php get_template_part('template/header','canvas'); ?>

    <!-- searchModal -->
    <div id="searchModal" class="uk-modal">
        <div class="uk-modal-dialog">
            <button type="button" class="uk-modal-close uk-close"></button>
            <div class="uk-modal-header uk-margin-bottom">
                <h2>
                    <i class="uk-icon uk-icon-search"></i>
                    Search
                </h2>
            </div>
            <input class="uk-form-large uk-width-1-1"
                   type="text"
                   name="search"
                   ng-model="searchQuery"
                   ng-minlength="3"
                   ng-keydown="searchPosts($event)"/>
            <ul class="uk-list">
                <li ng-repeat="result in searchResult">
                    <a href="{{result.permalink}}"
                       ng-bind-html="result.title">
                    </a>
                </li>
            </ul>
            <p class="uk-width-1-1 uk-text-center"
               ng-show="searchStart">
                <i class="uk-icon uk-icon-spin uk-icon-spinner uk-icon-large"></i>
            </p>
        </div>
    </div>
</div>
<!-- main body -->
<div id="dm-main-body"
     class="uk-width-1-1
            uk-margin-bottom"
     style="flex-grow:1">


