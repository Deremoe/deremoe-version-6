<?php get_header(); ?>
<script>
    var authorId = <?php print get_the_author_meta('ID'); ?>;
</script>
<link rel="stylesheet" 
      id="dm6-home-css" 
      href="<?php print Assets::css('home.css'); ?>">
<main id="dm6-main-body" 
      class=""
      ng-controller="authorCtrl">
    <div class="uk-container
                uk-container-center">
        
    <div class="uk-grid" 
         id="dm6-article-container"
         infinite-scroll="loadMorePost()"
         infinite-scroll-disabled='disableScroll'>
      
        <section class="uk-width-large-1-3 uk-margin-top uk-text-center">
              <div class="uk-thumbnail">
              <?php print get_avatar(get_the_author_meta('ID'),'512'); ?>
              </div>
        </section>
        <section class="uk-width-large-2-3 uk-width-small-1-1 uk-margin-top">
            <h1 class="uk-text-center-small uk-text-center-medium"
                ng-bind-html="author.author_name">
            </h1>
                <ul class="uk-list">
                    <li class="uk-margin-bottom uk-width-1-1">
                        <span class="uk-text-normal">
                            <span ng-if="author.twitter.length !== 0">
                                <i class="uk-icon uk-icon-twitter"></i>
                                <a href="http://twitter.com/{{author.twitter}}">
                                @{{author.twitter}}
                                </a> 
                            </span>
                            <span ng-if="author.user_email.length !== 0">
                            |
                            <i class="uk-icon uk-icon-envelope"></i>
                            <a href="mailto:{{author.user_email}}">
                            {{author.user_email}}
                            </a> 
                            </span>
                            <span ng-if="author.website.length !== 0">
                            |
                            <i class="uk-icon uk-icon-globe"></i>
                            <a ng-href="{{author.website}}">
                               {{author.website}}
                            </a>
                            </span>
                        </span>
                    </li>
                </ul>
                <div class="uk-width-1-1">
                    <p ng-bind-html="author.description">
					</p>
                </div>
        </section>
                
        <hr class="uk-grid-divider uk-width-1-1"
            />
        
        <!-- Ad Start -->
        <div class="uk-width-1-1">
        <?php get_template_part('template/ad','flat'); ?>
        </div>
        
        <!-- Loop 10 -->
        <item-basic item="post" ng-repeat="post in posts"></item-basic>
        
        <div class="uk-width-1-1 uk-text-center uk-margin-top" ng-show="disableScroll == true">
            <i class="uk-icon uk-icon-spin uk-icon-spinner uk-icon-large"></i>
        </div>
    </div>    
    </div>
</main>
<?php get_footer(); ?>