<?php 
    get_header();
    $cat = get_query_var('cat');
?>
<script>
    var catId       = <?php print $cat ?>;
    var catTitle    = "<?php single_cat_title('',true); ?>";
</script>
<link rel="stylesheet" 
      id="dm6-home-css" 
      href="<?php print Assets::css('home.css'); ?>">
<main id="dm6-main-body" 
      class=""
      ng-controller="categoryCtrl">

    <div class="uk-width-large-8-10 
				uk-width-medium-1-1 
				uk-container-center">
		<div class="uk-grid" 
             id="dm6-article-container"
             infinite-scroll="loadMorePost()"
             infinite-scroll-disabled='disableScroll'>
			<section class="uk-width-1-1 uk-text-center uk-margin-top uk-margin-bottom">
				<h1><?php single_cat_title('',true); ?></h1>
                <?php print category_description(); ?>
			</section>
			
            <!-- Ad Start -->
            <div class="uk-width-1-1
                        uk-margin-bottom">
            <?php get_template_part('template/ad','custom'); ?>
            </div>
                
			<!-- Loop 10 -->
            <item-basic item="post"
                        ng-repeat="post in posts"></item-basic>

            <div class="uk-width-1-1 uk-text-center uk-margin-top" ng-show="disableScroll == true">
                <i class="uk-icon uk-icon-spin uk-icon-spinner uk-icon-large"></i>
            </div>
		</div>
    </div>
</main>
<?php get_footer(); ?>