var app = angular.module('dm6',['infinite-scroll','ngSanitize'])
            .config(['$sceDelegateProvider',function($sceDelegateProvider){
                    $sceDelegateProvider.resourceUrlWhitelist([
                         'self',
                         "https://www.youtube.com/embed/**",
                         "http://www.youtube.com/embed/**",
                         "https://maps.googleapis.com/**",
                         "https://maps.google.com/**",
                         "https://www.facebook.com/video/**",
                    ]);
            }]);


