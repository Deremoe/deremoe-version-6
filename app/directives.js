app.directive('shareButton',function(){
    return {
        restrict:'A',
        link:function(scope,elem,att){
            var share = new Share(".share-button", {
                ui: {
                    flyout: 'top left'
                },
                networks: {
                      twitter: {
                        description:'Check out what I\'m reading on @Deremoe'
                      },
                      facebook: {
                        load_sdk: false,
                        caption: 'Check out what I\'m reading on Deremoe'
                      },
                      pinterest: {
                        //image:   // image to be shared to Pinterest [Default: config.image]
                        //description:    // text to be shared alongside your link to Pinterest [Default: config.description]
                      },
                      googlePlus: {
                      },                      
                      email: {
                      }
                }
              });
        }
    }
});

app.directive('catLink',['ajaxService',function(ajaxService){
    return {
        restrict:'A',
        link:function(scope,elem,att){
            var data = {
                action:'call_category_link',
                data:att.slug
            };
            ajaxService.getDataWithData(data).then(function(d){
                elem.attr('href',d.data.result);
            });
            //console.log(att.slug);
        }
    }
}]);

app.directive('clampThisText',function(){
    return {
        restrict:'A',
        link:function(scope,elem,att){
            $clamp(elem[0],att.line);
        }
    };
});

app.directive('itemBasic',function(){
    return {
        restrict    :'E',
        templateUrl :directiveUrl+'item-basic.html',
        replace     :true,
        scope       : {
            item: '='
        }
    }
});

app.directive('itemLarge',function(){
    return {
        restrict    :'E',
        templateUrl :directiveUrl+'item-large.html',
        replace     :true,
        scope       :{
            item:'='
        }
    }
});

app.directive('itemVideo',function(){
    return {
        restrict    :'E',
        templateUrl :directiveUrl+'item-video.html',
        replace     :true,
        scope       :{
            item:'='
        }
    }
});

app.directive('disclaimerModal',['$interval',function($interval){
    var link = function(scope,elem,attr){
        scope.displayDisclaimer = true;
        scope.disableButtons    = false;

        scope.continue = function(){
            //scope.timer             = 4;
            //Timer off
            scope.displayDisclaimer = false;
            /*
            $interval(function(){
                scope.disableButtons = true;
                scope.timer = scope.timer - 1;
                if(scope.timer === 0){
                    scope.displayDisclaimer = false;
                }
            },1000,4);
            */
        };
    };
    return {
        restrict    :'E',
        templateUrl :directiveUrl+'disclaimer-modal.html',
        replace     :true,
        link        :link
    }
}]);

app.directive('alert',[function(){
    return {
        restrict    :'E',
        replace     :true,
        transclude  :true,
        templateUrl :directiveUrl + 'alert.html',
        scope       :{
            type : '@'
        }
    };
}]);

app.directive('itemTrending',function(){
    return {
        restrict    :'E',
        replace     :true,
        templateUrl :directiveUrl + 'item-trending.html',
        scope       :{
            item:'='
        }
    }
});

app.directive('itemPlaceholder',function(){
    return {
        restrict    :'E',
        replace     :true,
        templateUrl :directiveUrl + 'item-placeholder.html',
        scope       :{
            type:'@'
        }
    }
});