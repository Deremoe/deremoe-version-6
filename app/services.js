app.factory('ajaxService',['$http',function($http){
               return {
                    getData:function(action){
                        var rsp = $http({
                            url:ajaxurl,
                            method:'GET',
                            params:{action:action}
                        });
                        return rsp;
                    },
                    getDataWithData:function(data){
                        var rsp = $http({
                            url:ajaxurl,
                            method:'GET',
                            params:{
                                action:data.action,
                                data:data.data
                            }
                        });
                        return rsp;
                    },
                    getDataWithMoreData:function(data){
                        var rsp = $http({
                            url:ajaxurl,
                            method:'GET',
                            params:data
                        });
                        return rsp;
                    }
               };
       }])
       .factory('scrollService',[function(){
               return{
                    scrollTrigger:function(scroll){
                        $(window).scroll(function(){
                            var scrollPosition = $(window).scrollTop();
                            var documentHeight = $(document).height();
                            var windowHeight   = $(window).height();

                            var scrollHeight = documentHeight - windowHeight;
                            var scrollPercent= (scrollPosition/scrollHeight)*100;
                            
                            if(scrollPercent > scroll.trigger)
                            {
                                scroll.callback();
                            } else {
                                scroll.fallback();
                            }
                        });
                    }
               };
       }])
       .factory('mobileDetectionService',[function(){
               return{
                   detectifMobile:function(){
                       if( navigator.userAgent.match(/Android/i)
                            || navigator.userAgent.match(/webOS/i)
                            || navigator.userAgent.match(/iPhone/i)
                            || navigator.userAgent.match(/iPad/i)
                            || navigator.userAgent.match(/iPod/i)
                            || navigator.userAgent.match(/BlackBerry/i)
                            || navigator.userAgent.match(/Windows Phone/i)
                            ){
                           return true;
                        }
                        return false;
                   }
               }
       }]);