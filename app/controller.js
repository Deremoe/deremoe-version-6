app.controller('headerCtrl',['$scope','ajaxService','$sce',function($scope,ajaxService,$sce){
    var authorList = {
        action:'call_author_list'
    };
    ajaxService.getDataWithMoreData(authorList).then(function(d){
        $scope.authorList = d.data.result;
    });
    
    $scope.searchPosts = function(keyPress){
        if(keyPress.keyCode === 13)
        {
            $scope.searchStart = true;
            var search = {
                action:'call_search',
                query:$scope.searchQuery
            }
            ajaxService.getDataWithMoreData(search).then(function(d){
                $scope.searchStart  = false;
                $scope.searchResult = d.data;
            });
        }
    }
    
}]);

app.controller('homeCtrl',['$scope','ajaxService','$sce',function($scope,ajaxService,$sce){
    $scope.disableScroll = false;
    if(angular.isDefined(Cookies.get('ctaModal'))){
        $scope.ctaModal = (Cookies.get('ctaModal') === 'false')?false:true;
    }
    else{
        $scope.ctaModal = true;
    }
    
    //close modal
    $scope.ctaModalClose = function(){
        //Modals will not Appear after 1 hour
        Cookies.set('ctaModal',false,{expires:6000});
        $scope.ctaModal = false;
    }

    $scope.loadAjaxFunc = function(){
        //Call Headline
        ajaxService.getData('call_headline').then(function(d){
            $scope.headline = {
                image       : d.data[0].feature,
                title       : d.data[0].title,
                permalink   : d.data[0].permalink,
                author      : d.data[0].author,
                date        : d.data[0].date,
                category    : d.data[0].category.cat_name
            };
        });

        //Call Series Review
        ajaxService.getData('call_reviews').then(function(d){
            var data       = d.data;
            $scope.reviews = data;
        });

        //Call Posts
        ajaxService.getData('call_posts').then(function(d){
            var data       = d.data;
            $scope.posts   = data;
        });

        //Call Video
        ajaxService.getData('call_video').then(function(d){
            var data       = d.data;
            $scope.yturl   = data[0].url;
            console.log(data[0].url);
        });

        //Call Video Links
        ajaxService.getData('call_home_videos_links').then(function(d){
            $scope.videos = d.data;
        });

        //Call Trending Event
        var trendingEvents = {
            action:'call_home_trending',
            option:'dm-trending-events'
        };
        ajaxService.getDataWithMoreData(trendingEvents).then(function(d){
            $scope.events = d.data;
        });

        var trendingTopics = {
            action:'call_home_trending',
            option:'dm-trending-topics'
        };
        ajaxService.getDataWithMoreData(trendingTopics).then(function(d){
            $scope.topics = d.data;
        });
    };

    var nextPostPage = 2;
    //Post Pagination
    $scope.loadMorePost = function(){
        $scope.disableScroll = true;
        var page = {
            action:'call_posts',
            data:nextPostPage
        };
        ajaxService.getDataWithData(page).then(function(d){
            $scope.disableScroll = false;
            var data       = d.data;
            angular.forEach(data,function(value,key){
                $scope.posts.push(value);
            });
            nextPostPage++;
        });
    }
}]);

app.controller('categoryCtrl',['$scope','ajaxService','$sce',function($scope,ajaxService,$sce){
    $scope.disableScroll = false;
    var categoryId = catId;
    var isReview   = (catTitle === 'Reviews')?true:false;
    var page       = 1;
    
    var category = {
        action:'call_category_loop',
        catId:categoryId,
        page:page
    };
    
    if(isReview === true){
        category.isReview = true;
    }
    
    ajaxService.getDataWithMoreData(category).then(function(d){
        $scope.posts = d.data;
    });
    
    $scope.loadMorePost = function(){
        category.page++;
        $scope.disableScroll = true;
        ajaxService.getDataWithMoreData(category).then(function(d){
            $scope.disableScroll = false;
            angular.forEach(d.data,function(value,key){
                $scope.posts.push(value);
            })
        });
    }
}]);

app.controller('singleCtrl',function(){});

app.controller('reviewCtrl',function(){});

app.controller('authorCtrl',['$scope','ajaxService','$sce',function($scope,ajaxService,$sce){
    $scope.disableScroll = false;
    
    var refAuthorId = authorId;
    var author = {
        action:'call_author_detail',
        authorId:refAuthorId
    };
    
    ajaxService.getDataWithMoreData(author).then(function(d){
        $scope.author = d.data;
    });
    
    var authorPosts = {
        action:'call_author_posts',
        authorId:refAuthorId,
        page:1
    }
    
    ajaxService.getDataWithMoreData(authorPosts).then(function(d){
        $scope.posts = d.data;
    });
    $scope.loadMorePost = function(){
        authorPosts.page++;
        $scope.disableScroll = true;
        ajaxService.getDataWithMoreData(authorPosts).then(function(d){
            $scope.disableScroll = false;
            angular.forEach(d.data,function(value,key){
                $scope.posts.push(value);
            });
        });
    };
}]);

app.controller('footerCtrl',['$scope','ajaxService','$sce',function($scope,ajaxService,$sce){
    var authorList = {
        action:'call_author_list'
    };
    
    ajaxService.getDataWithMoreData(authorList).then(function(d){
        $scope.authorList = d.data.result;
    });
}]);

app.controller('videosCtrl',['$scope','ajaxService','$sce',function($scope,ajaxService,$sce){
        $scope.disableScroll = false;
        
        var videos = {
            action:'call_video_loop',
            page:1
        };
        
        ajaxService.getDataWithMoreData(videos).then(function(d){
            $scope.posts = d.data;
        });
        
        var moreArticles = {
            action:'call_more_article'
        };
        ajaxService.getDataWithMoreData(moreArticles).then(function(d){
            $scope.moreArticles = d.data;
        });
        
        $scope.loadMorePost = function(){
            videos.page++;
            $scope.disableScroll = true;
            ajaxService.getDataWithMoreData(videos).then(function(d){
                $scope.disableScroll = false;
                angular.forEach(d.data,function(value,key){
                    $scope.posts.push(value);
                })
            });
        }
}]);

app.controller('landingEventCtrl',['$scope','ajaxService',function($scope,ajaxService){
    $scope.postId = null;
    $scope.posts   = {
        main_report     :null,
        video_links     :null,
        related_posts   :null,
        important_posts :null,
        map             :null
    };
    $scope.disableScroll = false;

    $scope.setValues = function(postID)
    {
        $scope.postId = parseInt(postID);
    };

    var loadAjaxFunc = function(){
        var loadEventReport = {
            action  :'call_report_loop',
            post_id :$scope.postId
        };

        ajaxService.getDataWithMoreData(loadEventReport).then(function(d){
            $scope.posts.main_report = d.data;
        });

        var loadEventVideos = {
            action: 'call_video_links',
            post_id:$scope.postId
        };
        ajaxService.getDataWithMoreData(loadEventVideos).then(function(d){
            $scope.posts.video_links = d.data;
        });

        var loadRelatedArticles = {
            action:'call_related_articles',
            post_id:$scope.postId,
            page:1
        }
        ajaxService.getDataWithMoreData(loadRelatedArticles).then(function(d){
            $scope.posts.related_posts = d.data;
        });

        $scope.loadMorePost = function(){
            loadRelatedArticles.page++;
            $scope.disableScroll = true;
            ajaxService.getDataWithMoreData(loadRelatedArticles).then(function(d){
                $scope.disableScroll = false;
                angular.forEach(d.data,function(value,key){
                    $scope.posts.push(value);
                });
            });
        };

        var loadImportantArticles = {
            action:'call_important_articles',
            post_id:$scope.postId
        }
        ajaxService.getDataWithMoreData(loadImportantArticles).then(function(d){
            $scope.posts.important_posts = d.data;
            console.log($scope.posts);
        });

        var loadMap = {
            action:'call_map_venue',
            post_id:$scope.postId
        }
        ajaxService.getDataWithMoreData(loadMap).then(function(d){
            $scope.posts.map = d.data;
        });
    };

    /*
     * Need to invoke watch as ajax function loads before the init value function is set.
     */
    $scope.$watch(function(){return $scope.postId},
                  function(){
                      loadAjaxFunc();
                  });
}]);

app.controller('landingTopicCtrl',['$scope','ajaxService',function($scope,ajaxService){
    $scope.postId = null;
    $scope.posts  = [];
    $scope.disableScroll = false;

    $scope.setPostId = function(postId){
        $scope.postId = parseInt(postId);
    };

    var loadAjaxFunc = function(){
        var loadRelatedArticle = {
            action  :'call_topic_related_article',
            post_id : $scope.postId
        };
        ajaxService.getDataWithMoreData(loadRelatedArticle).then(function(d){
            $scope.posts = d.data;
        });
    };

    $scope.$watch(function(){return $scope.postId},
                 function(){
                     loadAjaxFunc();
                 });
}]);

app.controller('archiveEventsCtrl',['$scope','ajaxService',function($scope,ajaxService){
    $scope.displaySearchResult = false;
    $scope.displayLoadingIcon  = false;
    $scope.displayEventItems   = true;

    $scope.searchEvent = function(keyPress){
        if(keyPress.keyCode === 13)
        {
            $scope.displayLoadingIcon = true;
            var search = {
                action:'call_event_search',
                query:$scope.searchEventQuery
            };
            ajaxService.getDataWithMoreData(search).then(function(d){
                $scope.displayLoadingIcon  = false;
                $scope.displaySearchResult = true;
                $scope.displayEventItems   = false;

                $scope.searchResults = d.data;
            });
        }
    };

    $scope.closeSearch = function(){
        $scope.searchResults = [];
        $scope.displaySearchResult = false;
        $scope.displayEventItems   = true;
    };

    var setLoop = {
        action:'call_event_loop',
        page:1
    };
    $scope.events = [];
    $scope.eventLoop = function(){
        $scope.displayLoadingIcon  = true;
        ajaxService.getDataWithMoreData(setLoop).then(function(d){
            $scope.events = $scope.events.concat(d.data);
            $scope.displayLoadingIcon  = false;
        });
    };

    $scope.eventMoreLoop = function(){
        setLoop.page++;
        $scope.displayLoadingIcon  = true;
        ajaxService.getDataWithMoreData(setLoop).then(function(d){
            $scope.events = $scope.events.concat(d.data);
            $scope.displayLoadingIcon  = false;
        });
    }
    $scope.eventLoop();
}]);

app.controller('archiveTopicCtrl',['$scope','ajaxService',function($scope,ajaxService){
    $scope.displaySearchResult = false;
    $scope.displayLoadingIcon  = false;
    $scope.displayTopicItems   = true;

    $scope.searchTopic = function(keyPress){
        if(keyPress.keyCode === 13)
        {
            $scope.displayLoadingIcon = true;
            var search = {
                action:'call_event_search',
                query:$scope.searchEventQuery
            };
            ajaxService.getDataWithMoreData(search).then(function(d){
                $scope.displayLoadingIcon  = false;
                $scope.displaySearchResult = true;
                $scope.displayTopicItems   = false;

                $scope.searchResults = d.data;
            });
        }
    };

    $scope.closeSearch = function(){
        $scope.searchResults = [];
        $scope.displaySearchResult = false;
        $scope.displayTopicItems   = true;
    };

    var setLoop = {
        action:'call_topic_loop',
        page:1
    };
    $scope.topics = [];
    $scope.topicLoop = function(){
        $scope.displayLoadingIcon  = true;
        ajaxService.getDataWithMoreData(setLoop).then(function(d){
            $scope.topics = $scope.topics.concat(d.data);
            $scope.displayLoadingIcon  = false;
        });
    };

    $scope.topicMoreLoop = function(){
        setLoop.page++;
        $scope.displayLoadingIcon  = true;
        ajaxService.getDataWithMoreData(setLoop).then(function(d){
            $scope.events = $scope.topics.concat(d.data);
            $scope.displayLoadingIcon  = false;
        });
    }
    $scope.topicLoop();
}]);

