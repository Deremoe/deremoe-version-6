<?php get_header(); ?>
<?php the_post(); ?>
<?php if(get_post_meta(get_the_ID(),'Optiondisclosure',true)):?>
    <disclaimer-modal></disclaimer-modal>
<?php endif; ?>
<link rel="stylesheet"
      id="dm6-single-css"
      href="<?php print Assets::css('single.css'); ?>"/>
<script src="<?php print Assets::js('vendor/share.js'); ?>"></script>
<main id="dm6-main-body"
      ng-controller="singleCtrl">
<div class="uk-width-large-6-10
            uk-width-medium-8-10
            uk-width-small-1-1
            uk-container-center" 
            itemscope itemtype="https://schema.org/BlogPosting">
    <header class="uk-margin-top" 
            id="dm6-single-header">
        <!-- Ad -->
        <div class="uk-text-center uk-visible-large">
            <?php get_template_part('template/ad','flat'); ?>
        </div>
        <div class="uk-text-center uk-hidden-large">
            <?php get_template_part('template/ad','tile'); ?>
        </div>

        <div class="dm6-category
                      uk-text-bold 
                      uk-margin-bottom
                      uk-margin-top
                      uk-text-center">
              <?php $cats = get_the_category(); foreach( $cats as $cat ) { ?>
                        <a href="<?php echo get_category_link($cat->term_id); ?>"><span><?php echo $cat->name; ?></span></a>
              <?php break;} ?>
        </div>

        <h1 class="uk-margin-remove uk-text-center" itemprop="name">
            <?php the_title(); ?>
        </h1>

        <p class="uk-margin
                  uk-text-center"
            >
            Posted <span itemprop="dateCreated"><?php the_time(); ?> <?php the_date(); ?></span> &bull;
            Updated <?php the_modified_date( 'G:i:s F d, Y','', '',true); ?>
        </p>
        <div class="dm6-meta uk-margin-top">
            <div class="uk-grid">
                <div class="uk-width-medium-2-3
                            uk-width-small-1-1
                            dm6-authors">
                    <ul class="uk-list">
                    <?php foreach(coAuthorsCustom() as $author): ?>
                        <li class="uk-margin-bottom uk-width-1-1">
                            <?php print get_avatar($author['author_id'],'25') ?>
                            <span class="uk-text-normal">
                                <a href="<?php print $author['author_url']; ?>"
                                   class="uk-link-muted"
								   itemprop="author" itemscope itemtype="http://schema.org/Person">
                                    <?php print $author['nice_name']; ?>
                                </a>
                            <?php if(!empty($author['twitter'])): ?>
                                |
                                <a href="http://twitter.com/<?php print $author['twitter']; ?>">
                                <i class="uk-icon uk-icon-twitter"></i>
                                @<?php print $author['twitter']; ?>
                                </a>
                            <?php endif; ?>
                            <?php if(!empty($author['email'])): ?>
                                |
                                <a href="mailto:<?php print $author['email']; ?>">
                                <i class="uk-icon uk-icon-envelope"></i>
                                Mail
                                </a>
                            <?php endif; ?>
                            </span>
                        </li>
                    <?php endforeach;?>
                    </ul>            
                </div>
                <div class="uk-width-medium-1-3
                            uk-width-small-1-1 uk-inline-block uk-float-right
                            dm6-share">
                    <div class="share-button uk-float-right" 
                         share-button></div>
                </div>
            </div>
            
        </div>
    </header>
    <div id="dm6-single-body" 
         class="uk-margin-top uk-text-break"
		 itemprop="articleBody">
        <?php the_content(); ?>
    </div>
	<hr />
    <?php print get_template_part('template/part','cta'); ?>
	<hr/>
  <div class="dm6-comments">
      <?php comments_template(); ?>
  </div>
</div>
</main>

<?php get_footer(); ?>