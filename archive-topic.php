<?php get_header(); ?>
<link rel="stylesheet"
      id="dm6-archive-events-css"
      href="<?php print Assets::css('archive-events.css'); ?>"/>
<main id="dm6-archive-events"
      class="uk-margin-top"
      ng-controller="archiveTopicCtrl">
<div class="uk-width-1-1"
     id="dm6-event-controls">
   <div class="uk-width-large-6-10
               uk-width-medium-8-10
               uk-width-small-1-1
               uk-container-center
               uk-text-center">
        <h1>Topics</h1>
        <p>This is where you can find our collection of posts related to a topic,
            together with information regarding them.</p>
        <p class="uk-margin-top">
            <input type="text"
                   placeholder="Type your query and press Enter"
                   ng-model="searchTopicQuery"
                   ng-keydown="searchEvent($event)"/></p>
   </div>
   <div class="uk-width-1-1
               uk-text-center"
        ng-show="displayLoadingIcon">
       <i class="uk-icon
                 uk-icon-spin
                 uk-icon-refresh
                 uk-icon-large"></i>
   </div>
   <!-- Search Result -->
   <div class="uk-width-1-1"
        ng-show="displaySearchResult">
       <div class="uk-width-medium-8-10
                    uk-width-small-1-1
                    uk-container-center">
           <p class="uk-text-center">
               <button class="uk-button uk-button-danger"
                       ng-click="closeSearch()">
                   <i class="uk-icon uk-icon-close"></i>
                   Close Search
               </button>
           </p>
           <div class="uk-grid">
            <item-trending item="topic"
                           ng-repeat="topic in searchResults"></item-trending>
           </div>
       </div>
   </div>

   <!-- Item Result -->
   <div class="uk-width-1-1
               uk-margin-top"
        infinite-scroll="eventMoreLoop()"
        infinite-scroll-disable="displayLoadingIcon"
        ng-show="displayEventItems">
        <div class="uk-width-medium-8-10
                    uk-width-small-1-1
                    uk-container-center">
        <div class="uk-grid">
            <item-trending item="topic"
                           ng-repeat="topic in topics"></item-trending>
        </div>
        </div>

   </div>

   <div class="uk-width-1-1
               uk-text-center"
        ng-show="displayLoadingIcon">
       <i class="uk-icon
                 uk-icon-spin
                 uk-icon-refresh
                 uk-icon-large"></i>
   </div>
</div>
</main>
<?php get_footer();?>