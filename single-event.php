<?php
    get_header();
    the_post();
?>
<div ng-controller="landingEventCtrl"
     ng-init="setValues(<?php print get_the_ID(); ?>)">
<?php get_template_part('template/landing','event'); ?>
</div>
<?php
    get_footer();
?>