<?php
/* Global Functions Only
 * 
 */

$DS = DIRECTORY_SEPARATOR;
require_once get_template_directory().$DS.'source'.$DS.'classes'.$DS.'assets.php';
require_once get_template_directory().$DS.'source'.$DS.'post_reviews.php';
require_once get_template_directory().$DS.'source'.$DS.'post_videos.php';
require_once get_template_directory().$DS.'source'.$DS.'post_event.php';
require_once get_template_directory().$DS.'source'.$DS.'post_topic.php';

require_once get_template_directory().$DS.'source'.$DS.'ajax'.$DS .'general.php';
require_once get_template_directory().$DS.'source'.$DS.'ajax'.$DS .'home.php';
require_once get_template_directory().$DS.'source'.$DS.'ajax'.$DS .'category.php';
require_once get_template_directory().$DS.'source'.$DS.'ajax'.$DS .'videos.php';
require_once get_template_directory().$DS.'source'.$DS.'ajax'.$DS .'author.php';
require_once get_template_directory().$DS.'source'.$DS.'ajax'.$DS .'events.php';
require_once get_template_directory().$DS.'source'.$DS.'ajax'.$DS .'topic.php';

require_once get_template_directory().$DS.'source'.$DS.'options'.$DS .'cta.php';
require_once get_template_directory().$DS.'source'.$DS.'options'.$DS .'pages.php';
require_once get_template_directory().$DS.'source'.$DS.'options'.$DS .'tinymce.php';
require_once get_template_directory().$DS.'source'.$DS.'options'.$DS .'trending.php';

require_once get_template_directory().$DS.'source'.$DS.'shortcodes'.$DS.'youtube.php';
require_once get_template_directory().$DS.'source'.$DS.'shortcodes'.$DS.'flickrit.php';
require_once get_template_directory().$DS.'source'.$DS.'shortcodes'.$DS.'uikit.php';

/*
 * Force SSL on everything
 */
function my_force_ssl() {
    return true;
}
add_filter('force_ssl', 'my_force_ssl', 10, 3);

/*
 * Add Thumbnail on RSS feed
 * http://www.codeinwp.com/blog/wordpress-image-code-snippets/
 */
add_filter('the_content_feed', 'rss_post_thumbnail');
function rss_post_thumbnail($content) {
  global $post;
  if( has_post_thumbnail($post->ID) )
    $content = '<p>' . get_the_post_thumbnail($post->ID, 'thumbnail') . '</p>' . $content;
  return $content;
}

/*
 * Remove fixed height on images
 * http://www.codeinwp.com/blog/wordpress-image-code-snippets/
 */
add_filter( 'post_thumbnail_html', 'remove_width_attribute', 10 );
add_filter( 'image_send_to_editor', 'remove_width_attribute', 10 );

function remove_width_attribute( $html ) {
   $html = preg_replace( '/(width|height)="\d*"\s/', "", $html );
   return $html;
}

/*
 * get feature image
 */
function get_that_image($post_id, $is_thumb = false)
{
    $image = '';
    if(has_post_thumbnail())
    {
        $image = wp_get_attachment_url(get_post_thumbnail_id($post_id));
    }
    else
    {
        global $post, $posts;
        ob_start();
        ob_end_clean();
        
        $output = preg_match_all('/<img.+src=[\'"]([^\'"]+)[\'"].*>/i', $post->post_content, $matches);
        $image	= $matches[1][0];
        $pattern	= '#(-([1-9][0-9]{3}|[0-9]{3})[xX](([1-9][0-9]{3}|[0-9]{3})))#';
        
        $result		= preg_match($pattern,$image,$match);
        
        if($result !== 0)
        {
            $image = ($thumb)?preg_replace('#'.$match[0].'#', '-290x290', $image):$image;
        }
    }
    return $image;
}

/*
 * Rating for Series Review
 */
function reviewRating($post_id)
{
    $review = get_post_meta($post_id,'d5-rating',true);
    $rating = array();
    for($x = 0; $x < 5; $x++)
    {
        if($x < $review)
        {
            $rating[] = 'uk-icon-star';
        }
        else
        {
            $rating[] = 'uk-icon-star-o'; 
        }
    }
    return $rating;
}

/*
 * Display the name and the avatar of the user
 */
if(class_exists('CoAuthorsIterator'))
{
    function coAuthorsCustom()
    {
        $authors    = new CoAuthorsIterator();
        $coAuthors  = array();
        $x          = 0;
        do{
            $current_author = $authors->current_author;
            if(!$authors->is_first())
            {
                $twitter = get_user_meta($current_author->ID,'twitter',true);
                $coAuthors[$x]['nice_name'] = $current_author->display_name;
                $coAuthors[$x]['author_id'] = $current_author->ID;
                $coAuthors[$x]['email'] = $current_author->user_email;
                $coAuthors[$x]['twitter']   = $twitter;
                $coAuthors[$x]['author_url']= get_author_posts_url($current_author->ID);
                $x++;
            }
        }while($authors->iterate());
        
        if($x == 0) {
            return null;
        }
        return $coAuthors;
    }
}

/*
 * Event Topic - Poster Image
 */
function getPosterImage($postId)
{
    $attachmentId = get_post_meta($postId,'eventPosterImage',true);
    return wp_get_attachment_image_src($attachmentId,'large')[0];
}

/*
 * Remove P tags from Images
 */
function filter_ptags_on_images($content){
    return preg_replace('/<p>\s*(<a .*>)?\s*(<img .* \/>)\s*(<\/a>)?\s*<\/p>/iU', '\1\2\3', $content);
}
add_filter('the_content', 'filter_ptags_on_images');

/*
 * Get Post Thumbnail
 */
function get_post_thumbnail($attachment_id,$size = "thumbnail")
{
    $post_thumbnail_id = wp_get_attachment_image_src($attachment_id,$size);
    return $post_thumbnail_id;
}

//Hook CSS and JS Here
function theme_default_styles(){
    wp_enqueue_script('jquery-main','//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.1/jquery.min.js');
    wp_enqueue_script('ui-kit-scirpt','//cdnjs.cloudflare.com/ajax/libs/uikit/2.21.0/js/uikit.min.js');
    wp_enqueue_script('ui-kit-sticky','//cdnjs.cloudflare.com/ajax/libs/uikit/2.21.0/js/components/sticky.min.js');
    wp_enqueue_script('angular-js','//cdnjs.cloudflare.com/ajax/libs/angular.js/1.4.1/angular.min.js');
    wp_enqueue_script('angulal-route','//cdnjs.cloudflare.com/ajax/libs/angular.js/1.4.1/angular-route.js');
    wp_enqueue_script('angular-sanitize','//cdnjs.cloudflare.com/ajax/libs/angular.js/1.4.1/angular-sanitize.min.js');
    
    //vendor scripts
    $template_uri = get_template_directory_uri().'/assets/js/vendor/';
    wp_enqueue_script('vendor-clamp',$template_uri.'clamp.js');
    wp_enqueue_script('vendor-cookie','//cdnjs.cloudflare.com/ajax/libs/Cookies.js/1.1.0/cookies.min.js');
    
    //angular derivatives
    $template_uri = get_template_directory_uri().'/app/';
    wp_enqueue_script('angular-core',$template_uri.'app.js');
    wp_enqueue_script('angular-controller',$template_uri.'controller.js');
    wp_enqueue_script('angular-directives',$template_uri.'directives.js');
    wp_enqueue_script('angular-services',$template_uri.'services.js');
    
    //angular vendor
    $template_uri = get_template_directory_uri().'/assets/js/vendor/';
    wp_enqueue_script('angular-infinitescroll',$template_uri.'angular-infinitescroll.js');
    
    
    //common styles
    $template_uri = get_template_directory_uri().'/assets/css/';
    wp_enqueue_style('Roboto-font','//fonts.googleapis.com/css?family=Roboto+Slab:700,400|Roboto:400,900,900italic,300');
    wp_enqueue_style('uikit-kore','//cdnjs.cloudflare.com/ajax/libs/uikit/2.21.0/css/uikit.min.css');
    wp_enqueue_style('theme-kore',$template_uri.'core.css');
    wp_enqueue_style('theme-directives',$template_uri.'directives.css');
	wp_enqueue_style('print',$template_uri.'print.css');
}
add_action('wp_enqueue_scripts','theme_default_styles');

//enable Ajax Functionality
function enable_ajax_functionality(){
    wp_localize_script('ajaxify',
                       'ajaxify_function',
                       array('ajaxurl' => admin_url('admin-ajax.php')));
}
add_action('template_redirect','enable_ajax_functionality');

//enable theme functionality
add_theme_support( 'post-thumbnails' );
//add_image_size('fm_featured_issue', 1920);
