</div>
<!-- main body -->
<footer id="dm6-main-footer"
        ng-controller="footerCtrl">
    <div class="uk-grid">
    <section class="uk-width-medium-4-10
                    uk-width-small-1-1"
             id="dm6-footer-red">
        <div class="uk-width-2-3 uk-float-right uk-hidden-small">
        <ul>
            <li class="uk-margin-bottom">
				<a href="<?php bloginfo('url'); ?>">
					<img class="uk-width-2-3" src="<?php print Assets::img('logo.svg'); ?>"/>
				</a>
            </li>
            <li>
                <a href="<?php print get_option('dm-about-page'); ?>">
					What is Deremoe?
				</a>
            </li>
            <li>
				<a href="<?php print get_option('dm-tac-page'); ?>">
					Terms of Use
				</a>
            </li>
            <li>
				<a href="<?php print get_option('dm-pp-page'); ?>">
					Privacy Policy
				</a>
            </li>
            <li>
				<a href="<?php print get_option('dm-coe-page'); ?>">
					Ethics Statement
				</a>
            </li>
        </ul>
        </div>
        
        <div class="uk-width-2-3 uk-visible-small">
        <ul>
            <li class="uk-margin-bottom">
				<a href="<?php bloginfo('url'); ?>">
					<img class="uk-width-1-2" src="<?php print Assets::img('logo.svg'); ?>"/>
				</a>
            </li>
             <li>
                <a href="<?php print get_option('dm-about-page'); ?>">
					What is Deremoe?
				</a>
            </li>
            <li>
				<a href="<?php print get_option('dm-tac-page'); ?>">
					Terms of Use
				</a>
            </li>
            <li>
				<a href="<?php print get_option('dm-pp-page'); ?>">
					Privacy Policy
				</a>
            </li>
            <li>
				<a href="<?php print get_option('dm-coe-page'); ?>">
					Ethics Statement
				</a>
            </li>
        </ul>
        </div>
    </section>
    <section class="uk-width-medium-6-10
                    uk-width-small-1-1"
             id="dm6-footer-black">
        <div class="uk-grid">
        <div class="uk-width-medium-1-4
                    uk-width-small-1-1
                    uk-hidden-medium">
            <ul class="uk-margin-remove">
                <li>
                <h2 class="uk-margin-remove">
                    Writers
                </h2>
                </li>
            </ul>
            <ul ng-bind-html="authorList" class="uk-margin-remove"></ul>
        </div>
            
        <div class="uk-width-medium-1-4
                    uk-width-small-1-1">
            <ul>
                <li>
                <h2 class="uk-margin-remove">
                    Follow
                </h2>
                </li>
                <li>
                    <a href="http://www.facebook.com/Deremoe" target="_blank">
                        <i class="uk-icon uk-icon-facebook"></i>
                        Facebook
                    </a>
                </li>
                <li>
                    <a href="http://twitter.com/Deremoe" target="_blank">
                        <i class="uk-icon uk-icon-twitter"></i>
                        Twitter
                    </a>
                </li>
                <li>
                    <a href="http://www.youtube.com/Deremoe" target="_blank">
                        <i class="uk-icon uk-icon-youtube-play"></i>
                        YouTube
                    </a>
                </li>
                <li>
                    <a href="http://plus.google.com/+Deremoe" target="_blank">
                        <i class="uk-icon uk-icon-google-plus"></i>
                        Google+
                    </a>
                </li>
                <li>
                    <a href="http://pinterest.com/Deremoe" target="_blank">
                        <i class="uk-icon uk-icon-pinterest"></i>
                        Pinterest
                    </a>
                <li>
                    <a href="http://radar.deremoe.com" target="_blank">
                        <i class="uk-icon uk-icon-tumblr"></i>
                        Tumblr
                    </a>
                </li>
                <li>
                    <a href="https://www.flickr.com/photos/deremoe" target="_blank">
                        <i class="uk-icon uk-icon-flickr"></i>
                        Flickr
                    </a>
                </li>     
				<li>
					<a href="http://instagram.com/deremoe" target="_blank">
						<i class="uk-icon uk-icon-instagram"></i>
						Instagram
					</a>
				</li>    	
				<li>
					<a href="http://medium.com/featured-on-deremoe" target="_blank">
						<i class="uk-icon uk-icon-medium-logo"></i>
                        Medium
                    </a>
				</li>       	                
                <li>
                    <a href="/rss">
                        <i class="uk-icon uk-icon-rss"></i>
                        RSS
                    </a>
                </li>
            </ul>
        </div>
            
        <div class="uk-width-medium-1-4
                    uk-width-small-1-1">
            <ul>
                <li>
                <h2 class="uk-margin-remove">
                    Partners
                </h2>
                </li>
                <li><a href="//animephproject.com/"
                       target="_new">
                        AnimePH
                    </a>
                </li>
                <li><a href="//otakista.com"
                       target="_new">
                        Otakista
                    </a>
                </li>
                <li><a href="//arkadymac.com"
                       target="_new">
                        Arkadymac
                    </a>
                </li>
                <li><a href="//fb.me/anitrendz"
                       target="_new">
                        Anime Trending
                    </a>
                </li>
                <li><a href="//fb.me/NarutoCosplayersPH"
                       target="_new">
                        NCPH
                    </a>
                </li>
                <li><a href="//facebook.com/groups/AnimeCornerGroupPilipinas"
                       target="_new">
                        ACGP
                    </a>
                </li>
            </ul>
        </div>
            
        <div class="uk-width-medium-1-4
                    uk-width-small-1-1">
            <ul>
                <li>
                <h2 class="uk-margin-remove">
                    Others
                </h2>
                </li>
                <li>
                    <a href="http://events.deremoe.com"
                       target="_new">Events Chart</a>
                </li>
                <li>
                    <a href="http://ficsmagazine.com"
                       target="_new">FiCS Magazine</a>
                </li>
            </ul>
        </div>
            
        <div class="uk-width-medium-1-1" id="dm6-copyright">
            <ul>
            <li>Copyright 2010-<?php print date('Y'); ?> | <a href="http://wordpress.org" target="_blank">Powered by WordPress</a> | Deremoe v6.2</li>
            </ul>
        </div>
        </div>
    </section>
    </div>
</footer>
<!-- Start of Async HubSpot Analytics Code -->
  <script type="text/javascript">
    (function(d,s,i,r) {
      if (d.getElementById(i)){return;}
      var n=d.createElement(s),e=d.getElementsByTagName(s)[0];
      n.id=i;n.src='//js.hs-analytics.net/analytics/'+(Math.ceil(new Date()/r)*r)+'/563648.js';
      e.parentNode.insertBefore(n, e);
    })(document,"script","hs-analytics",300000);
  </script>
<!-- End of Async HubSpot Analytics Code -->
<!-- dm6-main-container -->
<?php wp_footer(); ?>
</body>
</html>