<!DOCTYPE html>
<html <?php language_attributes(); ?> ng-app="dm6">
<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" 
          content="width=device-width, 
                   height=device-width, 
                   initial-scale=1.0, 
                   maximum-scale=1.0, 
                   user-scalable=no">
    
    <!-- FAVICON -->
    <link href="/favicon.ico" 
		  rel="shortcut icon" />	

    <!-- For iPad with high-resolution Retina display running iOS ≥ 7: -->
    <link rel="apple-touch-icon-precomposed" 
          sizes="152x152" 
          href="/apple-icon-152x152.png">

    <!-- For iPad with high-resolution Retina display running iOS ≤ 6: -->
    <link rel="apple-touch-icon-precomposed" 
          sizes="144x144" 
          href="/apple-icon-144x144.png">

    <!-- For iPhone with high-resolution Retina display running iOS ≥ 7: -->
    <link rel="apple-touch-icon-precomposed" 
          sizes="120x120" 
          href="/apple-icon-120x120.png">

    <!-- For iPhone with high-resolution Retina display running iOS ≤ 6: -->
    <link rel="apple-touch-icon-precomposed" 
          sizes="114x114" 
          href="/apple-icon-114x114.png">

    <!-- For first- and second-generation iPad: -->
    <link rel="apple-touch-icon-precomposed" 
          sizes="72x72" 
          href="/apple-icon-72x72.png">

    <!-- For non-Retina iPhone, iPod Touch, and Android 2.1+ devices: -->
    <link rel="apple-touch-icon-precomposed" 
          href="/apple-icon.png">
	<link rel="manifest" href="/manifest.json"/>
	
	<!-- For Windows 8 devices: -->
	<meta name="msapplication-TileColor" content="#ab1f2b"/>
	<meta name="msapplication-TileImage" content="/ms-icon-310x310.png"/>
	<meta name="theme-color" content="#ffffff"/>
    
    <title><?php wp_title('&bull;',true,'right'); ?></title>
    <?php wp_head(); ?>
    <script>
        var ajaxurl         = "<?php print admin_url('admin-ajax.php'); ?>";
        var directiveUrl    = "<?php print bloginfo('template_url').'/template/directives/'?>";
    </script>
</head>
<body class="uk-flex">
<!-- Header Item -->
<div ng-controller="headerCtrl"
     class="uk-width-1-1"
     id="dm-header-container">
    <header id="dm6-main-header"
            class="uk-width-1-1 dm6-main-header"
            data-uk-sticky>
        <div class="uk-container
                uk-container-center">
            <div class="uk-grid">
                <div id="dm6-main-brand"
                     class="uk-width-2-10
                        uk-visible-large">
                    <div class="dm6-header-container
                            uk-vertical-align">
                        <a href="<?php bloginfo('url'); ?>">
                            <img src="<?php print Assets::img('logo-title.svg'); ?>"/>
                        </a>
                    </div>
                </div>
                <div id="dm6-main-menu"
                     class="uk-width-8-10
                        uk-visible-large">
                    <div class="dm6-header-container
                            uk-vertical-align">
                        <div class="uk-vertical-align-middle uk-width-1-1">
                            <div class="uk-grid">
                                <div class="uk-width-4-6">
                                    <ul class="uk-subnav
                           uk-subnav-line
                           uk-margin-remove">
                                        <li data-uk-dropdown="{mode:'click'}">
                                            <a href=""
                                               cat-link slug="features">Features <i class="uk-icon uk-icon-caret-down"></i></a>
                                            <div class="uk-dropdown uk-dropdown-small">
                                                <ul class="uk-nav uk-nav-dropdown">
                                                    <li><a href="" cat-link slug="episode-blogging">Anime Blogging</a></li>
                                                    <li><a href="" cat-link slug="analysis">Analysis</a></li>
                                                    <li><a href="" cat-link slug="local-anime-magazine-industry">Anime Magazines</a></li>
                                                    <li><a href="" cat-link slug="manga-pitch">Manga Pitch</a></li>
                                                    <li><a href="" cat-link slug="tidbits">Tidbits</a></li>
                                                    <li><a href="" cat-link slug="opinions">Opinions</a></li>
                                                </ul>
                                            </div>
                                        </li>
                                        <li><a href="<?php print get_post_type_archive_link('d5-videos'); ?>">Video</a></li>
                                        <li><a href="" cat-link slug="reviews">Reviews</a></li>
                                        <li data-uk-dropdown="{mode:'click'}">
                                            <a href="">Authors <i class="uk-icon uk-icon-caret-down"></i></a>
                                            <div class="uk-dropdown uk-dropdown-small">
                                                <ul class="uk-nav uk-nav-dropdown" ng-bind-html="authorList">
                                                </ul>
                                            </div>
                                        </li>
                                        <li><a href="" cat-link slug="events">Events</a></li>
                                        <li data-uk-dropdown="{mode:'click'}">
                                            <a href="">
                                                <i class="uk-icon uk-icon-caret-down"></i>
                                            </a>
                                            <div class="uk-dropdown uk-dropdown-small">
                                                <ul class="uk-nav uk-nav-dropdown">
                                                    <li><a href="" cat-link slug="podcast">Netcast</a></li>
                                                    <li><a href="<?php print get_option('dm-partners-page'); ?>"
                                                           target="_new">Partners</a>
                                                    </li>
                                                    <li class="uk-nav-divider"></li>
                                                    <li class="uk-nav-header">Social</li>
													<li>
														<a href="http://www.facebook.com/Deremoe" target="_blank">
															<i class="uk-icon uk-icon-facebook"></i>
															Facebook
														</a>
													</li>
													<li>
														<a href="http://twitter.com/Deremoe" target="_blank">
															<i class="uk-icon uk-icon-twitter"></i>
															Twitter
														</a>
													</li>
													<li>
														<a href="http://www.youtube.com/Deremoe" target="_blank">
															<i class="uk-icon uk-icon-youtube-play"></i>
															YouTube
														</a>
													</li>
													<li>
														<a href="http://plus.google.com/+Deremoe" target="_blank">
															<i class="uk-icon uk-icon-google-plus"></i>
															Google+
														</a>
													</li>
													<li>
														<a href="http://pinterest.com/Deremoe" target="_blank">
															<i class="uk-icon uk-icon-pinterest"></i>
															Pinterest
														</a>
													</li>
													<li>
														<a href="http://radar.deremoe.com" target="_blank">
															<i class="uk-icon uk-icon-tumblr"></i>
															Tumblr
														</a>
													</li>
													<li>
														<a href="https://www.flickr.com/photos/deremoe" target="_blank">
															<i class="uk-icon uk-icon-flickr"></i>
															Flickr
														</a>
													</li>
													<li>
														<a href="http://instagram.com/deremoe" target="_blank">
															<i class="uk-icon uk-icon-instagram"></i>
															Instagram
														</a>
													</li>
													<li>
														<a href="http://medium.com/featured-on-deremoe" target="_blank">
															<i class="uk-icon uk-icon-medium-logo"></i>
															Medium
														</a>
													</li>       
                                                    <li class="uk-nav-divider"></li>
                                                    <li><a href="<?php print get_option('dm-pp-page'); ?>">
                                                            Privacy Policy</a>
                                                    </li>
                                                    <li><a href="<?php print get_option('dm-coe-page'); ?>">
                                                            Ethics Statement
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <div class="uk-width-2-6">
                                    <a href="#searchModal"
                                       class="uk-float-right"
                                       data-uk-tooltip="{pos:'left'}"
                                       title="Search The Site"
                                       data-uk-modal>
                                        <i class="uk-icon uk-icon-search"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>

    <!-- Header Mobile -->
    <header id="dm6-main-header-mobile"
            class="uk-hidden-large
               dm6-main-header
               uk-width-1-1"
            data-uk-sticky>
        <nav class="uk-navbar">
            <ul class="uk-navbar-nav">
                <li><a href="#dm-header-mobile"
                       data-uk-offcanvas><i class="uk-icon uk-icon-bars"></i></a></li>
                <li><a href="#searchModal"
                       class="uk-float-right"
                       data-uk-tooltip="{pos:'left'}"
                       title="Search The Site"
                       data-uk-modal>
                        <i class="uk-icon uk-icon-search"></i>
                    </a>
                </li>
            </ul>
            <div class="uk-navbar-flip">
                <div class="uk-navbar-brand">
                    <a href="<?php bloginfo('url'); ?>">
                        <img src="<?php print Assets::img('logo-title.svg'); ?>"/>
                    </a>
                </div>
            </div>
        </nav>
    </header>

    <!-- Mobile Header Menu -->
    <div id="dm-header-mobile"
         class="uk-offcanvas">
        <div class="uk-offcanvas-bar">
            <ul class="uk-nav uk-nav-offcanvas uk-nav-parent-icon"
                data-uk-nav>
                <li class="uk-nav-header">
                    Main Menu
                </li>
                <li class="uk-parent">
                    <a href="#">Features</a>
                    <ul class="uk-nav-sub">
                        <li><a href="" cat-link slug="episode-blogging">Anime Blogging</a></li>
                        <li><a href="" cat-link slug="analysis">Analysis</a></li>
                        <li><a href="" cat-link slug="local-anime-magazine-industry">Anime Magazines</a></li>
                        <li><a href="" cat-link slug="manga-pitch">Manga Pitch</a></li>
                        <li><a href="" cat-link slug="tidbits">Tidbits</a></li>
                        <li><a href="" cat-link slug="opinions">Opinions</a></li>
                    </ul>
                </li>
                <li><a href="<?php print get_post_type_archive_link('d5-videos'); ?>">Video</a></li>
                <li><a href="" cat-link slug="reviews">Reviews</a></li>
                <li class="uk-parent">
                    <a href="#">Authors</a>
                    <ul class="uk-nav-sub"
                        ng-bind-html="authorList">
                    </ul>
                </li>
                <li><a href="" cat-link slug="events">Event Reports</a></li>
                <li><a href="" cat-link slug="podcast">Podcast</a></li>
                <li><a href="<?php print get_option('dm-partners-page'); ?>"
                       target="_new">
                        Partners
                    </a>
                </li>
                <li><a href="<?php print get_option('dm-pp-page'); ?>"
                       target="_new">
                        Privacy Statement
                    </a>
                </li>
                <li><a href="<?php print get_option('dm-coe-page'); ?>"
                       target="_new">
                        Ethics
                    </a>
                </li>
                <li class="uk-nav-header">
                    Social
                </li>
                <li>
                    <a href="http://www.facebook.com/Deremoe" target="_blank">
                        <i class="uk-icon uk-icon-facebook"></i>
                        Facebook
                    </a>
                </li>
                <li>
                    <a href="http://twitter.com/Deremoe" target="_blank">
                        <i class="uk-icon uk-icon-twitter"></i>
                        Twitter
                    </a>
                </li>
                <li>
                    <a href="http://www.youtube.com/Deremoe" target="_blank">
                        <i class="uk-icon uk-icon-youtube-play"></i>
                        YouTube
                    </a>
                </li>
                <li>
                    <a href="http://plus.google.com/+Deremoe" target="_blank">
                        <i class="uk-icon uk-icon-google-plus"></i>
                        Google+
                    </a>
                </li>
                <li>
                    <a href="http://pinterest.com/Deremoe" target="_blank">
                        <i class="uk-icon uk-icon-pinterest"></i>
                        Pinterest
                    </a>
                </li>
                <li>
                    <a href="http://radar.deremoe.com" target="_blank">
                        <i class="uk-icon uk-icon-tumblr"></i>
                        Tumblr
                    </a>
                </li>
                <li>
                    <a href="https://www.flickr.com/photos/deremoe" target="_blank">
                        <i class="uk-icon uk-icon-flickr"></i>
                        Flickr
                    </a>
                </li>
                <li>
                    <a href="http://instagram.com/deremoe" target="_blank">
                        <i class="uk-icon uk-icon-instagram"></i>
                        Instagram
                    </a>
                </li>
                <li>
                    <a href="http://medium.com/featured-on-deremoe" target="_blank">
                        <i class="uk-icon uk-icon-medium-logo"></i>
                        Medium
                    </a>
                </li>                
            </ul>
        </div>
    </div>

    <!-- searchModal -->
    <div id="searchModal" class="uk-modal">
        <div class="uk-modal-dialog">
            <button type="button" class="uk-modal-close uk-close"></button>
            <div class="uk-modal-header uk-margin-bottom">
                <h2>
                    <i class="uk-icon uk-icon-search"></i>
                    Search
                </h2>
            </div>
            <input class="uk-form-large uk-width-1-1"
                   type="text"
                   name="search"
                   ng-model="searchQuery"
                   ng-minlength="3"
                   ng-keydown="searchPosts($event)"/>
            <ul class="uk-list">
                <li ng-repeat="result in searchResult">
                    <a href="{{result.permalink}}"
                       ng-bind-html="result.title">
                    </a>
                </li>
            </ul>
            <p class="uk-width-1-1 uk-text-center"
               ng-show="searchStart">
                <i class="uk-icon uk-icon-spin uk-icon-spinner uk-icon-large"></i>
            </p>
        </div>
    </div>
</div>
<!-- main body -->
<div id="dm-main-body"
     class="uk-width-1-1
            uk-margin-bottom"
     style="flex-grow:1">


